#!/usr/bin/perl

use strict; use warnings;
use lib qw(
    /usr/share/uuidhamster/mojolicious/lib
    /usr/share/uuidhamster/api/lib
);

use Util::APIKeyGenerator;
use YAML::Syck;

my $config_file = '/etc/uuidhamster/api.conf';
if (! ( -f $config_file || -W $config_file ) )  {
    printf "File %s does not exist or is not writeable by this user (%s)\n", $config_file, (getpwuid $>);
    exit 1;
}

my $config;
eval {
    $config = YAML::Syck::LoadFile($config_file);
};
if ($@) {
    printf "Could not load configuration file %s\n", $config_file;
    exit 1;
}

my $key = undef;

while (1) {
    $key = Util::APIKeyGenerator::generate();
    if (!(grep /$key/, @{$config->{api_keys}})) {
        push @{$config->{api_keys}}, $key;
        last;
    }
}

eval {
    YAML::Syck::DumpFile($config_file, $config);
};
if ($@) {
    printf "Could not save configuration file: %s\n", $@;
    exit 1;
}

printf "API key has been generated and saved to file %s\n%s\n", $config_file, $key;
exit 0
