#!/usr/bin/perl

use strict; use warnings;

BEGIN {
    unshift @INC, "/usr/share/uuidhamster/api/lib";
    unshift @INC, "/usr/share/uuidhamster/mojolicious/lib";
}

my $VERSION = '{$project}.{$version}';

use Mojolicious::Lite;
use Mojo::JSON;

use TryCatch;
use Data::Dumper;

use Profile;
use Util::APIKeyGenerator;
use Util::UUIDUtil;

use Exception::NoSuchUserException;
use Exception::NoSuchUUIDException;
use Exception::TooManyRequestsException;
use Exception::InvalidAPIKeyException;
use Exception::AuthorizationException;

my $json    = JSON::XS->new()->utf8;

my $default_config = {
    hypnotoad       => {
        listen          => [ 'http://127.0.0.1:1337' ],
        workers         => 10,
        pid_file        => '/var/run/uuidhamster/api.pid',
    },
    secrets         => [],
    log             => {
        path            => '/var/log/uuidhamster/api.log',
        level           => 'info',
    },
    application     => {
        mrl_monitor_host    => 'http://127.0.0.1:1336',
        api_keys        => [
            Util::APIKeyGenerator::generate()
        ],
        cors_origins    => [
            'http://localhost',
            'https://localhost',
        ],
    },
    role            => {
        default         => 1,
        'query.single'  => [],
        'query.bulk'    => [],
        save            => [],
        delete          => [],
        status          => [],
    },
    cache           => {
        use_uuid            => 1,
        use_username        => 1,
        use_alternatives    => 0,
        use_unknown         => 1,
    },
    resolver        => {
        username_search_interval    => 60 * 60 * 24 * 7 * 4,    # 4 weeks default interval
        max                         => 1,                       # maximum search results
    },
    hamster         => {
        lifetime            => 60 * 60 * 24 * 7,            # 1 week lifetime
        soft_set            => 0,                           # no need for memcached
        driver              => 'Hamster::Driver::Memcached',# we prefer memcached as backend
        autoinit            => 0,                           # no need for memcached
    },
};

# configure application
plugin Config => {
    file    => '/etc/uuidhamster/api.conf',
    default => $default_config,
};

# configure secrets
app->secrets((ref app->config->{secrets} eq "ARRAY") ? app->config->{secrets} : [ app->config->{secrets} ]);

# configure logging
app->log(Mojo::Log->new(
    path        => app->config->{log}->{path}   || undef,
    level       => app->config->{log}->{level}  || 'info',
));

# load plugins
plugin 'Plugin::Hamster';
plugin 'Plugin::MojangAPI';
plugin 'Plugin::RESTClient';
plugin 'Plugin::Resolver';
plugin 'Plugin::RoleManager';

helper 'is_exception' => sub {
    shift;  # remove $self
    return (shift =~ /^Exception::.+Exception$/);
};

sub error {
    my ($self, $exception) = @_;

    $self->app->log->error(sprintf('%s: %s', $exception->class(), $exception->message()));
    $self->render(
        status  => $exception->code(),
        json    => {
            code    => $exception->code(),
            class   => $exception->class(),
            message => $exception->message(),
        },
    );
}

helper 'ratelimit.add' => sub {
    my ($self) = @_;
    $self->restclient->request('POST', sprintf('%s/add', app->config->{application}->{mrl_monitor_host}), undef, { 'Accept' => 'application/json' });
};

helper 'ratelimit.status' => sub {
    my ($self) = @_;
    my $response = $self->restclient->request('GET', sprintf('%s/status', app->config->{application}->{mrl_monitor_host}), undef, { 'Accept' => 'application/json' });
    return Mojo::JSON::decode_json($response->{content});
};

hook before_routes => sub {
    my ($self) = shift;

    # check api key
    my $key         = $self->tx->req->headers->header('X-HamsterAPI-Key');
    my $valid_key   = 0;

    foreach (@{$self->config->{application}->{api_keys}}) {
        $valid_key = (defined $key && $key eq $_);
    }

    return error($self, new Exception::InvalidAPIKeyException($key)) unless $valid_key;

    # set response header for CORS requests that do not support preflight requests
    $self->res->headers->header('Access-Control-Allow-Origin' => join(",", @{$self->config->{application}->{cors_origins}}));
};


# answer to preflight requests (CORS)
options '*' => sub {
    my $self = shift;

    $self->res->headers->header('Access-Control-Allow-Credentials'  => 'true');
    $self->res->headers->header('Access-Control-Allow-Methods'      => 'OPTIONS, GET, POST, DELETE, PUT');
    $self->res->headers->header('Access-Control-Allow-Headers'      => 'Accept', 'Content-Type', 'X-CSRF-Token', 'X-HamsterAPI-Key');
    $self->res->headers->header('Access-Control-Max-Age'            => '1728000');

    $self->respond_to(any => { data => '', status => 200 });
};

# returns a profile for a given uuid or username
get '/profile/:ident' => sub {
    my ($self) = shift;
    my $ident = $self->stash('ident');

    # send 301: moved permanently and redirect to destination
    $self->res->code(301);

    if (Util::UUIDUtil::is_UUID($self->stash('ident'))) {
        $self->redirect_to(sprintf '/profile/uuid/%s', $self->stash('ident'));
    } else {
        $self->redirect_to(sprintf '/profile/username/%s', $self->stash('ident'));
    }
};

# returns a profile for a given uuid
get '/profile/uuid/:uuid' => sub {
    my ($self)  = shift;
    my $uuid    = $self->stash("uuid");

    try {
        die new Exception::AuthorizationException() unless $self->role->has($self->tx->req->headers->header('X-HamsterAPI-Key'), 'query.single');
        die new Exception::IllegalArgumentException('uuid') if (!defined $uuid || $uuid eq '');

        $self->app->log->info(sprintf('Requesting profile for UUID %s', $uuid));
        my $profile = $self->resolve->uuid($uuid);

        # render to client
        $self->respond_to(
            json    => { json   => $profile->serialize() },
            any     => { data   => Dumper($profile->serialize()) },
        );

    } catch (Exception::BasicException $e) {
        error($self, $e);
    };
};

# returns a set of profiles for a given username
get '/profile/username/:username' => sub {
    my ($self)      = shift;
    my $username    = $self->stash('username');
    my $at          = $self->param('at');
    my $max         = $self->param('max');
    $at ||= time();

    try {
        die new Exception::AuthorizationException() unless $self->role->has($self->tx->req->headers->header('X-HamsterAPI-Key'), 'query.single');
        die new Exception::IllegalArgumentException('username') if (!defined $username || $username eq '');

        $self->app->log->info(sprintf('Requesting profile of username %s (at %s - max: %s)', $username, $at, (!defined $max || $max == 0) ? 'no maximum' : $max));
        my $profiles = $self->resolve->usernames($username, $at, $max);

        # render to client
        $self->respond_to(
            json    => { json   => $profiles },
            any     => { data   => Dumper($profiles) },
        );

    } catch (Exception::BasicException $e) {
        error($self, $e);
    };
};


# returns an array of profiles for given uuids/usernames (in request body)
post '/profiles' => sub {
    my ($self) = shift;

    my $usernames = Mojo::JSON::decode_json($self->req->body);
    try {
        die new Exception::AuthorizationException() unless $self->role->has($self->tx->req->headers->header('X-HamsterAPI-Key'), 'query.bulk');
        die new Exception::IllegalArgumentException('usernames') if (!defined $usernames || ref $usernames ne "ARRAY");

        $self->app->log->info(sprintf('Bulk-requesting %d profiles', scalar @$usernames));
        my $profiles = $self->resolve->bulk($usernames);

        # render to client
        $self->respond_to(
            json    => { json   => $profiles },
            any     => { data   => Dumper($profiles) },
        );

    } catch (Exception::BasicException $e) {
        error($self, $e);
    };
};

# commits changes that have been soft-set'ed by the hamster
put '/save' => sub {
    my ($self) = shift;

    try {
        die new Exception::AuthorizationException() unless $self->role->has($self->tx->req->headers->header('X-HamsterAPI-Key'), 'save');

        $self->cache->uuidhamster->commit();

    } catch (Exception::BasicException $e) {
        return error($self, $e);
    }

    $self->respond_to(
        json    => { json   => 1 },
        any     => { data   => 'success' },
    );
};

# delete an entry in the hamsters cheeks
del '/:ident' => sub {
    my ($self)  = shift;
    my $ident   = $self->stash('ident');

    try {
        die new Exception::AuthorizationException() unless $self->role->has($self->tx->req->headers->header('X-HamsterAPI-Key'), 'delete');

        (Util::UUIDUtil::is_UUID($ident))
            ? $self->cache->uuidhamster->delete->uuid($ident)
            : $self->cache->uuidhamster->delete->username($ident);

        $self->respond_to(
            json    => { json   => 1 },
            any     => { data   => 'success' },
        );

    } catch (Exception::BasicException $e) {
        return error($self, $e);
    };
};

get '/status' => sub {
    my ($self) = shift;

    try {
        die new Exception::AuthorizationException() unless $self->role->has($self->tx->req->headers->header('X-HamsterAPI-Key'), 'status');

        my $status = $self->ratelimit->status();
        $self->respond_to(
            json    => { json   => $status },
            any     => { data   => Dumper($status) },
        );

    } catch (Exception::BasicException $e) {
        return error($self, $e);
    };
};

app->start;
