package Profile;

use strict; use warnings;

use JSON::XS;
use Util::UUIDUtil;
use Carp;

sub new {
    my ($class, $args) = @_;

    my $self = bless {
        uuid        => undef,
        username    => undef,
        cracked     => undef,
        legacy      => undef,
        history     => [],
    }, $class;

    $self->set_profile($args) if defined $args;

    return $self;
}

sub getUsername {
    return shift->{username};
}

sub getUUID {
    return shift->{uuid};
}

sub set_profile {
    my ($self, $args) = @_;

    foreach (keys %$args) {
        $self->{$_} = ($_ eq 'uuid') ? Util::UUIDUtil::to_UUID($args->{$_}) : $args->{$_};
    }
    return $self;
}

sub set_history {
    my ($self, $history) = @_;
    croak unless ref $history eq 'ARRAY';

    foreach (@$history) {
        $self->add_history($_->{name}, $_->{changedToAt});
    }
    return $self;
}

sub add_history {
    my ($self, $username, $changed_at) = @_;
    
    push @{$self->{history}}, {
        username    => $username,
        changed_at  => $changed_at,
    };
}

sub serialize {
    my ($self) = @_;

    my $return = {};
    foreach (keys %$self) {
        $return->{$_} = $self->{$_};
    }
    return $return;
}

1;
