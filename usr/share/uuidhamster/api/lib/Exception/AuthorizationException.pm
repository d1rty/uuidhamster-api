package Exception::AuthorizationException;
use base 'Exception::BasicException';

sub new {
    my ($class) = @_;
    return $class->SUPER::new('You are not allowed to perform this operation');
}

sub code {
    return 401;
}

1;
