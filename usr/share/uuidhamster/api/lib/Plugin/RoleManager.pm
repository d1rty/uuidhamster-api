package Plugin::RoleManager;
use Mojo::Base 'Mojolicious::Plugin';

sub register {
    my ($self, $app) = @_;

    $app->helper('role.has' => sub {
        my ($c, $ident, $role) = @_;

        if (
            defined $c->app->config->{role}->{$role} && 
            ref $c->app->config->{role}->{$role} eq 'ARRAY' && 
            scalar @{$c->app->config->{role}->{$role}}> 0 
        ) {
            return grep /$ident/, @{$c->app->config->{role}->{$role}};
        }

        my $d = $c->app->config->{role}->{default} || 0;
        return $d;
    });

}

1;
