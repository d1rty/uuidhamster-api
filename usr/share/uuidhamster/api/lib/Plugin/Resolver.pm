package Plugin::Resolver;
use Mojo::Base 'Mojolicious::Plugin';

use TryCatch;

sub register {
    my ($self, $app) = @_;

    $app->helper('resolve.uuid' => sub {
        my ($c, $uuid) = @_;

        #### check cache policies

        # use_unknown policy
        die new Exception::NoSuchUUIDException($uuid) if $c->cache->unknown->has($uuid);

        # use_uuid policy
        my $cache_data = $c->cache->uuidhamster->get->uuid($uuid);
        if (defined $cache_data) {
            return new Profile()->set_profile($cache_data);
        }


        #### retrieve history data from mojang api

        my $uuid_history = $c->mojang->uuid_history($uuid);
        die $uuid_history if $c->is_exception($uuid_history);

        my $profile = new Profile();
        $profile->set_history($uuid_history);

        # find earliest username
        my $username;
        my $changed_at = time();

        if (scalar @$uuid_history == 1) {
            $username = $uuid_history->[0]->{name};
        } else {
            foreach (@$uuid_history) {
                if (defined $_->{changedToAt}) {
                    $username = $_->{name};
                    $changed_at = $_->{changedToAt};
                }
            }
        }

        die new Exception::NoSuchUUIDException($uuid) unless defined $username;

        my $profile_data;
        try {
            # retrieve profile data from mojang api with first changed_at date
            $profile_data = $c->mojang->profile($username, $changed_at);

        } catch (Exception::NoSuchUserException $e) {
            $c->cache->unknown->set($username);
            die $e;
        };

        $profile->set_profile($profile_data);

        $c->cache->uuidhamster->set->uuid($profile);
        $c->cache->uuidhamster->set->username($profile);
        $c->cache->uuidhamster->set->alternatives($profile);

        return $profile;
    });


    $app->helper('resolve.usernames' => sub {
        my ($c, $username, $at, $max) = @_;
        $at     ||= time();
        $max    ||= 0;

        my $profiles = [];

        die new Exception::NoSuchUserException($username) if $c->cache->unknown->has($username);

        my $cache_data = $c->cache->uuidhamster->get->username($username);
        if ($max != 0 && defined $cache_data) {
            push @$profiles, new Profile()->set_profile($cache_data)->serialize();
        }

        if (!scalar @$profiles || scalar @$profiles < $max) {
            my $abort_date = Plugin::MojangAPI::critical_date() - $c->config->{resolver}->{username_search_interval};
            for (my $i = $at; $i > $abort_date; $i -= $c->config->{resolver}->{username_search_interval}) {

                my $profile = new Profile();

                try {
                    my $data = $c->mojang->profile($username, $i);
                    next unless defined $data;
        
                    $profile->set_profile($data);

                } catch (Exception::NoSuchUserException $e) {
                    $c->app->log->warn($e->message());
                    next;
                } catch (Exception::IllegalArgumentException $e) {
                    $c->app->log->warn($e->message());
                    next;
                };
               

                # check if we already have this profile
                my $next = 0;
                foreach (@$profiles) {
                    if ($_->{uuid} eq $profile->{uuid}) {
                        $next = 1;
                        last;
                    }
                }
                next if $next;

                try {
                    # request uuid username history from mojang
                    my $profile_uuid_history = $c->mojang->uuid_history($profile->{uuid});
                    $profile->set_history($profile_uuid_history);

                } catch (Exception::NoSuchUUIDException $e) {
                    $c->app->log->error($e->message());
                    $c->app->log->error(sprintf("Could not retrieve history for UUID %s", $profile->{uuid}));
                    $c->app->log->error('Mojang returned a username history before, but has no UUID associated with one of those usernames');
                    $c->app->log->error('This should rarely happen, mostly in cases like a user deleted his account (and Mojang fucked up of course)');
                    next;
                } catch (Exception::IllegalArgumentException $e) {
                    $c->app->log->error($e->message());
                    next;
                };


                $c->cache->uuidhamster->set->uuid($profile);
                $c->cache->uuidhamster->set->username($profile);
                $c->cache->uuidhamster->set->alternatives($profile);

                push @$profiles, $profile->serialize();


                # check max parameter
                if (
                    ($max > 0 && scalar @$profiles >= $max) ||
                    ($max == 0 && defined $c->config->{resolver}->{max} && $c->config->{resolver}->{max} > 0 && scalar @$profiles >= $c->config->{resolver}->{max})
                ) {
                    $c->app->log->debug(sprintf('A Profile resultset reached maximum size (%d). Skipping further requests', $max));
                    last;
                }


            } # // for (my $i = $at; $i > $abort_date; $i -= $c->config->{resolver}->{username_search_interval}) {
        } # // if (!scalar @$profiles)

        # throw exception if array is empty
        if (scalar @$profiles == 0) {

            # policy use_unknown
            $c->cache->unknown->set($username);            

            my $e = new Exception::NoSuchUserException($username);
            $c->app->log->error(sprintf('No profile found for username %s', $username));
            die $e;
        }

        return $profiles;
    });


    $app->helper('resolve.bulk' => sub {
        my ($c, $usernames) = @_;
        my ($profiles, $missing_usernames) = ([], []);

        # check which usernames we have to request from mojang api
        foreach (@$usernames) {
            if (defined (my $hamster_data = $c->hamster->get($_))) {
                push @$profiles, $hamster_data;
            } else {
                push @$missing_usernames, $_;
            }
        }

        my $bulk_profile_data = $c->mojang->bulk_profiles($missing_usernames);

        foreach (@$bulk_profile_data) {
            my $profile = new Profile($_);

            try {
                $profile->set_history($c->mojang->uuid_history($_->{uuid}));
            } catch (Exception::IllegalArgumentException $e) {
                $c->app->log->error($e->message());
                next;
            } catch (NoSuchUUIDException $e) {
                $c->app->log->error($e->message());
                next;
            };

            $c->cache->uuidhamster->set->uuid($profile);
            $c->cache->uuidhamster->set->username($profile);
            $c->cache->uuidhamster->set->alternatives($profile);
    
            push @$profiles, $profile->serialize();
        }

        return $profiles;
    });
}

1;
