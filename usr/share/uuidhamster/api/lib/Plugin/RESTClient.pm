package Plugin::RESTClient;
use Mojo::Base 'Mojolicious::Plugin';

use REST::Client;

our $client;

sub get_client {
    my ($self, $c) = @_;

    $client = REST::Client->new() unless defined $client;
    return $client;
}

sub register {
    my ($self, $app) = @_;

    $app->helper('restclient.request' => sub {
        my ($c, @args) = @_;

        my $response = $self->get_client()->request(@args);
        return {
            code    => $response->responseCode(),
            content => $response->responseContent(),
        };
    });
}

1;
