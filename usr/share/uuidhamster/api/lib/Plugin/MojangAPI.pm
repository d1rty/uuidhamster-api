package Plugin::MojangAPI;
use Mojo::Base 'Mojolicious::Plugin';

use TryCatch;

use Mojo::JSON;

use Exception::TooManyRequestsException;
use Exception::NoSuchUserException;
use Exception::NoSuchUUIDException;
use Exception::IllegalArgumentException;

sub hostname {
    return 'https://api.mojang.com';
}

sub headers {
    return {
        'Content-Type'  => 'application/json',
    };
}

sub critical_date {
    # Wed Feb  4 00:00:00 CET 2015
    return 1423004400;
}

sub _normalize {
    my ($self, $data) = @_;

    my $is_array = (ref $data eq 'ARRAY');
    $data = [ $data ] unless $is_array;

    foreach (@$data) {
        $_->{uuid}      = delete $_->{id};
        $_->{username}  = delete $_->{name};
        $_->{cracked}   = (defined $_->{demo})      ? 1 : 0;
        $_->{legacy}    = (defined $_->{legacy})    ? 1 : 0;

        delete $_->{demo};
    }
    
    return ($is_array) ? $data : $data->[0];
}

sub register {
    my ($self, $app) = @_;

    $app->helper('mojang.profile' => sub {
        my ($c, $username, $timestamp) = @_;

        die new Exception::IllegalArgumentException('username') unless defined $username;
        $timestamp ||= time();

        $c->app->log->info(sprintf('Calling Mojang API: Profile for username %s (time: %s)', $username, $timestamp));
        $c->ratelimit->add();

        my $response = $c->restclient->request('GET', sprintf('%s/users/profiles/minecraft/%s?at=%d', $self->hostname(), $username, $timestamp), undef, $self->headers());
        return $self->_normalize(Mojo::JSON::decode_json($response->{content}))
            if $response->{code} eq '200';

        if (length($response->{content}) == 0) {
            die new Exception::NoSuchUserException($username);
        }
        die new Exception::TooManyRequestsException();
    });

    $app->helper('mojang.uuid_history' => sub {
        my ($c, $uuid) = @_;

        $uuid = Util::UUIDUtil::to_lazy_UUID($uuid);
        die new Exception::IllegalArgumentException($uuid) unless defined $uuid;

        $c->app->log->info("Calling Mojang API: history for UUID $uuid");
        $c->ratelimit->add();

        my $response = $c->restclient->request('GET', sprintf('%s/user/profiles/%s/names', $self->hostname(), $uuid), undef, $self->headers());
        return Mojo::JSON::decode_json($response->{content})
            if $response->{code} eq '200';

        if (length($response->{content}) == 0) {
            die new Exception::NoSuchUUIDException($uuid);
        }
        die new Exception::TooManyRequestsException();
    });

    $app->helper('mojang.bulk_profiles' => sub {
        my ($c, $usernames) = @_;

        die new Exception::IllegalArgumentException('usernames') unless defined $usernames;

        $usernames = [ $usernames ] unless ref $usernames eq 'ARRAY';

        $c->app->log->info(sprintf('Calling Mojang API: Bulk requesting %d profile(s)', scalar @$usernames));
        $c->ratelimit->add();

        my $response = $c->restclient->request('POST', sprintf('%s/profiles/minecraft', $self->hostname()), Mojo::JSON::encode_json($usernames), $self->headers());
        return $self->_normalize(Mojo::JSON::decode_json($response->{content}))
            if $response->{code} eq '200';

        die new Exception::TooManyRequestsException();
    });

}

1;
