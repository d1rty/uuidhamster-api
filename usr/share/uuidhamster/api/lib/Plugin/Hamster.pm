package Plugin::Hamster;
use Mojo::Base 'Mojolicious::Plugin';

use TryCatch;
use Exception::CachePolicyException;

use Hamster::GenericHamster;

my $hamsters = {
    uuidhamster => undef,
    unknown     => undef,
};

sub get_hamster {
    my ($self, $c, $alias) = @_;

    if (!defined $hamsters->{$alias}) {
        $hamsters->{$alias} = new Hamster::GenericHamster({
           alias       => $alias,
           driver      => $c->config->{hamster}->{driver}      || undef,           # detect best driver
           lifetime    => $c->config->{hamster}->{lifetime}    || 86400 * 7,       # 1 week default lifetime
           soft_set    => $c->config->{hamster}->{soft_Set}    || 0,
           autoinit    => $c->config->{hamster}->{autoinit}    || 0,
       });
    }

    return $hamsters->{$alias};
}

sub cache_defaults {
    return {
        uuid            => 1,
        username        => 1,
        alternatives    => 0,
        unknown         => 1,
    };    
}

sub register {
    my ($self, $app) = @_;

    # configuration values
    foreach my $scope (('uuid', 'username', 'alternatives', 'unknown')) {
        $app->helper(sprintf('cache.use.%s', $scope) => sub {
            my ($c) = shift;

            return $c->param(sprintf('use_%s', $scope)) if (defined $c->param(sprintf('use_%s', $scope)));
            return $c->config->{cache}->{sprintf('use_%s', $scope)} || $self->cache_defaults()->{$scope};
        });
    }


    # auto generated primary cache for player profiles
    foreach my $operation (('set', 'get', 'delete')) {
        foreach my $scope (('uuid', 'username', 'alternatives')) {

            next if ($scope eq "alternatives" && ($operation eq "get" || $operation eq "delete"));

            $app->helper(sprintf('cache.uuidhamster.%s.%s', $operation, $scope) => sub {
                my ($c) = shift;

                unless ($c->cache->use->$scope) {
                    my $e = new Exception::CachePolicyException(
                        sprintf('use_%s', $scope),
                        sprintf('Policy denies execution of "%s.%s" operation', $operation, $scope)
                    );
                    $c->app->log->warn(sprintf('[cache:uuidhamster] %s', $e->message()));
                    return undef;
                }

                if ($scope eq "alternatives") {
                    my ($profile) = @_;

                    my $alternatives = [ grep defined, map {
                        ($_->{username} eq $profile->{username}) ? undef : $_->{username}
                    } @{$profile->{history}} ];

                    if (!scalar @$alternatives) {
                        $c->app->log->debug(sprintf('[cache:uuidhamster] No alternative usernames associated with UUID %s, not saving alternative usernames', $profile->{uuid}));
                    }
                    return undef;
                }


                if ($operation eq "get" || $operation eq "delete") {
                    my ($ident) = shift;

                    $c->app->log->debug(sprintf('[cache:uuidhamster] Executing "%s.%s" operation on ident "%s"', $operation, $scope, $ident));
                    return $self->get_hamster($c, 'uuidhamster')->get($ident);

                } elsif ($operation eq "set") {
                    my ($profile) = @_;

                    $c->app->log->debug(sprintf('[cache:uuidhamster] Executing "set.%s" operation for profile (UUID: %s - username: %s)',
                        $scope,
                        $profile->{uuid},
                        $profile->{username},
                    ));
                    return $self->get_hamster($c, 'uuidhamster')->set($profile->{scope}, $profile->serialize());
                } else {

                }
            });
        }
    }

    $app->helper('cache.uuidhamster.commit' => sub {
        my ($c) = shift;

        $c->app->log->debug('[cache:uuidhamster] Executing "commit" operation');
        return $self->get_hamster($c, 'uuidhamster')->commit();
    });



    ##### secondary cache for unknown idents #####
    foreach my $operation (('has', 'set', 'delete')) {
        $app->helper(sprintf('cache.unknown.%s', $operation) => sub {
            my ($c, $ident) = @_;

            if (!$c->cache->use->unknown) {
                my $e = new Exception::CachePolicyException('use_unknown', sprintf('Policy denies operation "%s"', $operation));
                $c->app->log->warn(sprintf('[cache:unknown] %s', $e->message()));
                return 0;
            }

            if ($operation eq 'has') {
                $c->app->log->debug(sprintf('[cache:unknown] Executing "has" operation for ident %s', $ident));
                return defined $self->get_hamster($c, 'unknown')->get($ident);
            }

            if ($operation eq 'set') {
                $c->app->log->debug(sprintf('[cache:unknown] Executing "set" operation for ident %s', $ident));
                return $self->get_hamster($c, 'unknown')->set($ident, 1);
            }

            if ($operation eq 'delete') {
                $c->app->log->debug(sprintf('[cache:unknown] Executing "delete" operation for ident %s', $ident));
                return $self->get_hamster($c, 'unknown')->delete($ident);
            }
        });
    }

}

1;
