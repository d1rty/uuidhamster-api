package Util::APIKeyGenerator;

use strict; use warnings;
use Util::ExecUtil;
use Carp;

my $util = Util::ExecUtil->new();

sub generate {
    my $key = [];
    foreach my $i (qw(3 2 2 2 5)) {
        my $result = $util->execute(sprintf "od -vAn -N%d -tx1 /dev/urandom | tr -cd 0123456789abcdef", $i);
        croak sprintf "Error while generating key: %s\n", $result->{stderr} unless $result->{success};
        push @$key, $result->{stdout};
    }
    return join "-", @$key;
}

1;
