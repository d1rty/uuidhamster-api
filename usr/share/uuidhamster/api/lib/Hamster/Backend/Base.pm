package Hamster::Backend::Base;

=head1 NAME

Hamster::Backend::Base


=head1 SYNOPSIS

  package Hamster::Backend::MyBackend;

  use Hamster::Backend::Base;
  use base qw(Hamster::Backend::Base);


=head1 DESCRIPTION

This is a base package for all backend packages. It provides some default methods to be compatible with the backend interface package.

=cut


use strict; use warnings;
use BaseClass;
use base qw(BaseClass);

use Hamster::Interface::UICL;


=head1 CONSTRUCTOR

=over 4

=item C<new>

Creates a new instance of the backend package.

=back

=cut
sub new {
    my ($class, $args) = @_;

    my $self = bless {
        name            => $class,
        uicl            => undef, 
        hamster         => $args->{hamster},
        test_backend    => $args->{test_backend}    || 0,
    }, $class;

    return $self;
}


=head1 METHODS

=over 4

=item C<initialize>

  if (!$backend->initialize()) {
      print STDERR $backend->get_error();
  }

Initialize the Backend package (must use UICL packages)

=back

=cut
sub initialize {
    my ($self) = shift;

    $self->debug("Initializing UICL Interface with ".$self->getUICL());

    $self->{uicl} = Hamster::Interface::UICL->new( {
        uicl        => $self->getUICL(),
        test_uicl   => $self->{test_backend},
    } );

    if ($self->{uicl}->has_error()) {
        $self->register_error("Could load load UICL Interface: ".$self->{uicl}->get_error());
        return 0;
    }

    if (!$self->{uicl}->initialize()) {
        $self->register_error($self->{uicl}->get_error());
        return 0;
    }

    if (!$self->{test_backend}) {
        $self->debug("Omitting Backend test");
    } else {   
        $self->debug("Testing Backend");
        if (!$self->test()) {
            $self->register_error($self->get_error());
            return 0;
        }
    }

    return 1;
}


=over 4

=item C<test>

  if (!$backend->test()) {
      print STDERR $backend->get_error();
  }

Default test method, that always returns true (1). You may want to overwrite it in your child package to do some tests.

=back

=cut
sub test {
    my ($self) = @_;
    $self->debug("Backend tested successfully");
    return 1;
}


=over 4

=item C<get_backend_name>

  my $backend_name = $backend->get_backend_name();

Returns the name of the backend package

=back

=cut
sub get_backend_name {
    my ($self) = @_;
    return $self->{name};
}


=head1 DEPENDENCIES 

This package does not have any dependencies.


=head1 ERROR HANDLING

This package uses L<BaseClass> for error reporting and handling. If an error happened you may check $backend->has_error().
The error message itself can be obtained from $backend->get_error().

For more information read the L<BaseClass documentation|BaseClass>.


=head1 AUTHOR

Tristan Cebulla <tristan.cebulla@1und1.de>, 1&1 Internet AG


=head1 BUGS

There are no known bugs.


=head1 COPYRIGHT & LICENSE

Copyright (c) 2011 Tristan Cebulla (<tristan.cebulla@1und1.de>). All rights reserved.

This module is free software; you can redistribute it and/or modify it under the same terms as
Perl itself. See L<perlartistic>.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=cut



1;
