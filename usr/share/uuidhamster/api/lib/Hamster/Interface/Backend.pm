package Hamster::Interface::Backend;

=head1 NAME

Hamster::Interface::Backend


=head1 SYNOPSIS
  
  use Hamster::Interface::Backend;

  my $backend_interface = Hamster::Interface::Backend->new( {
      hamster   => $hamster->get_hamster_name(),
      backends  => @arrayref,
  } );


=head1 DESCRIPTION

This is the Hamster backend interface package. It will try to load, instanciate and test a series of backend packages, if they have been provided via $self->{backends} which must be an arrayref. If this arrayref does not contain any backend package names, it is assumed that the Hamster provides a C<fill_cheek()> method itself. In that case no backend operations are possible.

Otherwise all backend operations (methods) are called via Perls' AUTOLOAD magic. Data mining is done via the backend packages' C<fill_cheek()> method.


=cut

use strict; use warnings;
use BaseClass;
use base qw(BaseClass);

use Carp;


=head1 METHODS

=over 4

=item C<new>

Creates a new instance of the backend interface package.

=back

=cut
sub new {
    my ($class, $args) = @_;

    my $self = bless {
        hamster         => $args->{hamster},
        backends        => $args->{backends},
        backend         => undef,
        has_backend     => 0,
        test_backend    => $args->{test_backend} || 0,
    }, $class;

    return $self;
}


=over 4

=item C<initialize>

Initializes the backend package (if at least one was provided). If no backend package was provided true (1) is returned. In that case we assume that the Hamster has its own C<fill_cheek()> method.

In any other case the backend interface tries to load, instanciate and test the backend package. If one of these steps fail this method returns false. In addition an error message is available.

If the backend package could have beend loaded, instanciated and tested successfully true (1) is returned. In any other cases false (0) will be returned.

=back

=cut
sub initialize {
    my ($self) = @_;

    my $last_error = undef;
    my $backend;

    # no backends to initialize, Hamster has the old-style fill_cheek() method, return true (1)
    if (scalar $#{$self->{backends}} eq -1) {
        $self->debug($self->{hamster}." does not use a Backend/UICL Interface");
        return 1;
    }

    foreach $backend (@{$self->{backends}}) {
        my $backend_file = $backend.".pm";
        $backend_file =~ s{::}{/}g;
        $self->debug("Probing Backend $backend from '$backend_file'");
        eval {
            require $backend_file;
        };
        if ($@) {
            # try next backend, but save last error message in case no backend package can be found
            $last_error = "Could not load Backend: $@";
            $self->register_error($last_error);
            next;
        }

        # try instanciating the backend
        $self->debug("Instanciating Backend");
        eval {
            $self->{backend} = $backend->new( {
                hamster         => $self->{hamster},
                test_backend    => $self->{test_backend},
            } );
        };
        if ($@) {
            # try next backend, but save last error message in case no backend package can be found
            $last_error = "Could not instanciate Backend': $@";
            $self->register_error($last_error);
            next;
        }

        # try to initialize backend
        $self->debug("Initializing Backend");
        if (!$self->{backend}->initialize()) {
            $last_error = "Could not initialize Backend: ".$self->{backend}->get_error();
            $self->register_error($last_error);
            next;
        } else {
            $self->debug("Backend ready");
            $self->{has_backend} = 1;
            $self->reset_error();
            return 1;                
        }
    }


    # in case we dont have a backend package available
    if (!$self->{has_backend}) {
        # check if there is a backend package object instanciated and if this package has an error
        if ( (defined $self->{backend}) && ($self->{backend}->has_error()) ) {
            $self->register_error("Backend throwed error: ".$last_error);
            return 0;
        } else {
            $self->register_error($self->{hamster}." seems to have no backend available.");
            return 0;
        }
    }

    if ($self->has_error()) {
        # register an error in case the last backend could not be initialized
        $self->register_error("Could not load suiting Backend: $last_error");
        return 0;
    }
}


=over 4

=item C<has_backend>

  my $data;
  if ($backend_interface->has_backend()) {
      $data = $backend_interface->fill_cheek($ident);
  } else {
      $data = $hamster->fill_cheek($ident);
  }

Returns a boolean value to determine if a backend package could has been loaded and is available via the backend interface.

=back

=cut
sub has_backend {
    my ($self) = @_;
    return $self->{has_backend};
}


=over 4

=item C<get_backend_name>

  my $name = $backend_interface->get_backend_name();

Returns the backend packages' name.

=back

=cut
sub get_backend_name {
    my ($self) = @_;
    return undef unless defined $self->{backend};
    return $self->{backend}->get_backend_name();
}


=over 4

=item C<fill_cheek>

This method just calls the backend packages' C<fill_cheek()> method and returns in return vale.

=back

=cut
sub fill_cheek {
    my ($self, $ident) = @_;

    return undef unless defined $ident;
    return $self->{backend}->fill_cheek($ident);
}


=over 4

=item C<AUTOLOAD>

  my $result = $backend_interface->backend_method();

Overloads the Hamster backend interface package with the backend method.

=back

=cut
sub AUTOLOAD {
    our $AUTOLOAD;
    my ($self, $ident, $args) = @_;
    $ident ||= $self->{ident};

    my $backend_method = ($AUTOLOAD =~ m{([^:]+$)})[0];

    # exit the program here, because the Hamsters backend package is not fully functional (someone 'forgot' something)
    if (!$self->{backend}->can($backend_method)) {
        confess ("Method '$backend_method' does not exist in backend '".$self->get_backend_name()."'.");
    }

    return $self->{backend}->$backend_method($ident, $args);
}


=pod Package destruction (needed by AUTOLOAD)
=cut
sub DESTROY {
    my ($self) = @_;
    undef $self;
}


=head1 DEPENDENCIES 

  Carp


=head1 ERROR HANDLING

This package uses L<BaseClass> for error reporting and handling. If an error happened you may check $backend->has_error().
The error message itself can be obtained from $backend->get_error().

For more information read the L<BaseClass documentation|BaseClass>.


=head1 AUTHOR

Tristan Cebulla <tristan.cebulla@1und1.de>, 1&1 Internet AG


=head1 BUGS

There are no known bugs.


=head1 COPYRIGHT & LICENSE

Copyright (c) 2011 Tristan Cebulla (<tristan.cebulla@1und1.de>). All rights reserved.

This module is free software; you can redistribute it and/or modify it under the same terms as
Perl itself. See L<perlartistic>.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=cut

1;
