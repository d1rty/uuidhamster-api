package Hamster::Interface::Driver;

=head1 NAME

Hamster::Interface::Driver


=head1 SYNOPSIS

  use Hamster::Interface::Driver;

  my $interface = Hamster::Interface::Driver->new( {
      driver      => undef,       # A driver to use forcely
      soft_set    => 0,           # soft_set is disabled by default
      hamster     => "",          # The Hamsters package name
      lifetime    => 86400,       # A default lifetime (86400 seconds)
  } );

  $interface->flush();


=head1 DESCRIPTION

The Hamster driver interface provides access to a series of drivers that create, read, update and delete (CRUD)
data in a storage system.

Note that the drivers' C<set> method will be fed with the hamsterified cheek data, the C<hamsterify_cheek_data()> method returns.

=head1 DEPENDENCIES 

This package does not have real dependencies since it only operates a series of drivers. The dependencies are
specified in the driver package itself.


=head1 ERROR HANDLING

This package uses L<BaseClass> for error reporting and handling. If an error happened you may check $interface->has_error().
The error message itself can be obtained from $interface->get_error().

For more information read the L<BaseClass documentation|BaseClass>.

=cut

use strict; use warnings;
use BaseClass;
use base qw(BaseClass);

=head1 DEFAULT INTERFACE DRIVERS

The interface supports three default drivers (in exactly this order):

=over 4

=item Hamster::Driver::Memcached

This driver uses Cache::Memcached as storage backend

=item Hamster::Driver::YAML*

These drivers use YAML/YAML::Syck as storage backend

=item Hamster::Driver::Temp

This driver uses a simple hash as data storage

=back

=cut
our @drivers = qw(
    Hamster::Driver::Memcached
    Hamster::Driver::YAMLXS
    Hamster::Driver::YAMLSyck107
    Hamster::Driver::YAMLSyck
    Hamster::Driver::YAML
    Hamster::Driver::Temp
);


=head1 METHODS

=over 4

=item C<new>

Create a new instance of this package. The constructor supports the following hash-args:

=over 4

=item hamster (String)

The Hamsters package name, e.g. Hamster::TestHamster. This parameter is needed.

=item lifetime (Integer)

The cheek lifetime. It defaults to 86400 seconds. This parameter is optional.

=item driver (String)

The storage driver name (e.g. Hamster::Driver::MyOwnDriver) to use explicitly. No other drivers are probed.
Note that this will cause your Hamster to have a driver-dependent fill_cheek method depending on the drivers shortname. This parameter is optional.

=item soft_set (Boolean)

Use the soft_set/commit methods. This can be useful when using the Hamster::Driver::YAML driver to create auto-references by the YAML engine.
This means that all entries are written at once and not sequentially. This parameter is optional.

=back

=back

=cut
sub new {
    my ($class, $args) = @_;

    my $self = bless {
        driver          => undef,
        force_driver    => $args->{driver}      || undef,
        hamster         => $args->{hamster},
        lifetime        => undef,
        soft_set        => undef,
        ident           => $args->{ident},
        test_driver      => $args->{test_driver}  || 0,
    }, $class;

    $self->set_lifetime($args->{lifetime});
    $self->set_soft_set($args->{soft_set});

    return $self;
}


=over 4

=item C<initialize>

  if (!$interface->initialize()) {
      print STDERR $interface->get_error();
  }

Initialize the interface. If a single driver is specified (via $self->{force_driver} or { driver => "" } at instanciation) only this driver is probed.
Otherwise all other default drivers are probed. The first succeeding driver is taken as storage system.

At first the driver package pis required and an object is instanciated. If this succeeds the initialize() and test() methods are called.

=over 4

=item C<initialize()>

The driver needs to be initialized. The method returns true (1) if the driver has been initialized successfully.

=item C<test()>

The driver methods needs to be tested. Only if these tests did not fail the driver is ok to get used.

=back

If both methods succeed, the driver is ok to be used. Otherwise the next driver is probed.
If no more driver is left to probe the error message of the last probed driver is available.
Typically this error will never happen, because the last driver is Hamster::Driver::Temp which uses a hash data structure as a storage engine. This should always work. Note that this driver does only contain data as long the object of the corresponding Hamster that uses this storage system exists. After undefining this object, all data are lost.

If the driver initialization and test failed this method returns false (0), otherwise true (1).

=back

=cut
sub initialize {
    my ($self) = @_;
    my $last_error = undef;

    # add the forced driver name at the beginning of the array to try this driver first
    if (defined $self->{force_driver}) {
        unshift(@drivers, $self->{force_driver});
        $self->debug("Forced to use the driver '".$self->{force_driver}."'");
    }

    # probe all drivers, consume drivers that don't work
    my $driver;
    while ($driver = shift @drivers) {
        my $driver_file = $driver.".pm";
        $driver_file =~ s{::}{/}g;

        $self->debug("Probing Driver $driver from '$driver_file'");

        eval {
            require $driver_file;
        }; 
        if ($@) {
            $last_error = "Could not load Driver: $@";
            $self->register_error($last_error);

            # try next driver if no forced driver was defined
            next unless defined $self->{force_driver};

            # forced driver could not be loaded, abort
            $last_error = "Could not load forced Driver: $@";
            $self->register_error($last_error);
            last;
        }

        # try instancing the driver
        $self->debug("Instanciating Driver");
        eval {
            $self->{driver} = $driver->new( {
                hamster     => $self->{hamster},
                lifetime    => $self->{lifetime},
                soft_set    => $self->{soft_set},
                DEBUG       => $self->{DEBUG},
            } );
        };

        # error while trying to instanciate the driver, go on with next driver
        if ($@) {
            $last_error = "Could not instanciate Driver: $@";
            $self->register_error($last_error);

            next unless defined $self->{force_driver};

            $last_error = "Could not instanciate forced Driver: $@";
            $self->register_error($last_error);
            last;
        }

        $self->debug(sprintf("Configuring Driver with ident %s", (defined $self->{ident}) ? $self->{ident} : "uninitialized" ));
        # configure the driver
        if (!$self->{driver}->configure($self->{ident})) {
            $last_error = $self->{driver}->get_error();
            next unless defined $self->{force_driver};
            $last_error .= " (FORCED)"; last;
        }

        # driver could not be initialized
        if (!$self->{driver}->initialize()) {
            $last_error = "Could not initialize Driver: ".$self->{driver}->get_error();
            $self->register_error($last_error);

            next unless defined $self->{force_driver};

            $last_error = "Could not initialize forced Driver: ".$self->{driver}->get_error();
            $self->register_error($last_error);
            last;
        }

        if (!$self->{test_driver}) {
            $self->debug("Omitting Driver test");
        } else {
            if (!$self->{driver}->test()) {
                $last_error = "Could not test Driver: ".$self->{driver}->get_error();
                $self->register_error($last_error);

                next unless defined $self->{force_driver};

                $last_error = "Could not test forced Driver: ".$self->{driver}->get_error();
                $self->register_error($last_error);
                last;
            }
        }

        # success, unshift the current driver to get used by the next Hamster
        unshift (@drivers, $driver);
        $self->reset_error();
        $self->debug("Driver ready");
        return 1;
    }
    
    $self->register_error("Could not load suiting Storage Driver '$driver': $last_error");
    return 0;
}


=over 4

=item C<get_lifetime>

  my $lifetime = $interface->get_lifetime();

Return the cheek lifetime in seconds.

=back

=cut
sub get_lifetime {
    my ($self) = @_;
    return $self->{lifetime};    
}


=over 4

=item set_lifetime

  $interface->set_lifetime($lifetime);

Set the cheek lifetime (default is 86400 seconds).

=over 4

=item $lifetime (Integer)

The lifetime of cheek data in seconds.

=back

=back

=cut
sub set_lifetime {
    my ($self, $lifetime) = @_;
    $lifetime ||= 86400;
    $self->{lifetime} = $lifetime;
}


=over 4

=item C<get_driver_name>

  my $driver_shortname = $interface->get_driver_name();

Return the drivers package name.

=back

=cut
sub get_driver_name {
    my ($self) = @_;
    return $self->{driver}->get_driver_name();
}


=over 4

=item C<set_soft_set>

  $interface->set_soft_set(1);  

Enable or disable the the I<soft_set> feature via true (1) or false (0).

=back

=cut
sub set_soft_set {
    my ($self, $soft_set) = @_;
    $soft_set ||= 0;
    $self->{soft_set} = $soft_set;
}


=over 4

=item C<get_soft_set>

  if ($interface->get_soft_set()) {
      $interface->commit();
  }

Returns boolean if the I<soft_set> feature is used (1) or not (0).

=back

=cut
sub get_soft_set {
    my ($self) = @_;
    return $self->{soft_set};
}


=over 4

=item C<hamsterify_cheek_data>

  my $normalized_data = $driver->hamsterify_cheek_data($data);

Normalizes data to the Hamsters cheek data format. This consists of the following hashref data structure:

  {
      data          => $data,
      creation_date => time(),
  }

=back

=cut
sub hamsterify_cheek_data {
    my ($self, $data) = @_;

    $self->debug("Hamsterifying cheek data");
    return {
        data            => $data,
        creation_date   => time(),
    };
}


=over 4

=item C<AUTOLOAD>

  my $result = $interface->get($ident);

Magic method to provide access to driver CRUD methods.

Note that the I<soft_set> feature is used automatically, depending on the boolean return value of C<$self->{soft_set}>. The advantage of this feature is that you need to decide only once (at the Hamsters initialization) whether you want to use the I<soft_set/commit> features or not. Also note, that you B<must> hit the C<commit> method by yourself when using the I<soft_set> functionality.

=over 4

=item $ident

The ident string that identifies this cheek data.

=item $data

The data belonging to the ident string (used for set and update methods). This may contain any data structure you want.

=back

All these methods (except for get) need to return true (1) or false (0) to check if the storage operation has been successful or not.

=back

=cut
sub AUTOLOAD {
    our $AUTOLOAD;
    my ($self, $ident, $data) = @_;
    $ident ||= $self->{ident};

    # determine interface method
    my $interface_method = ($AUTOLOAD =~ m{([^:]+$)})[0];

    $self->debug("Accessing Driver Interface: ".$interface_method."('$ident') from ".(caller)[0]." in line ".(caller)[2]);

    # hamsterify cheek data if we hit the set or update commands
    $data = $self->hamsterify_cheek_data($data) if (grep $interface_method eq $_, qw(set soft_set update));

    # check if we need to use set or soft_set
    if ( ($self->{soft_set}) && ($interface_method eq "set") ) {
        $self->debug("soft_set enabled");
        $interface_method = "soft_set";
    }

    # call the method and return, also check for errors and register if there is any
    my $result = $self->{driver}->$interface_method($ident, $data);
    $self->register_error($self->{driver}->get_error()) if ($self->{driver}->has_error());
    return $result;
}


=pod Package destruction (needed by AUTOLOAD)
=cut
sub DESTROY {
    my ($self) = @_;
    undef $self;
}


=head1 AUTHOR

Tristan Cebulla <tristan.cebulla@1und1.de>, 1&1 Internet AG


=head1 BUGS

There are no known bugs.


=head1 COPYRIGHT & LICENSE

Copyright (c) 2011 Tristan Cebulla (<tristan.cebulla@1und1.de>). All rights reserved.

This module is free software; you can redistribute it and/or modify it under the same terms as
Perl itself. See L<perlartistic>.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=cut

1;
