package Hamster::Interface::UICL;

=head1 NAME

Hamster::Interface::UICL

=head1 DESCRIPTION

The UICR interface is the United Internet Customer Lookup interface package. This interface serves a lot of SOAP-, Socket- or other-based packages that gathers customer related information.

These packages are mainly used by the Hamster backend packages, but they can be used easily by third parties.

=cut


use strict; use warnings;
use BaseClass;
use base qw(BaseClass);


=head1 CONSTRUCTOR

=cut
sub new {
    my ($class, $args) = @_;

    my $self = bless {
        name        => $class,
        uicl        => undef,
        package     => $args->{uicl},
        test_uicl   => $args->{test_uicl}   || 0,
    }, $class;

    $self->register_error("No UICL Interface package defined") unless defined $self->{package};
    return $self;
}


=head1 METHODS

=over 4

=item C<initialize>

=back

=cut
sub initialize {
    my ($self) = shift;
    my $uicl_file = $self->{package}.".pm";
    $uicl_file =~ s{::}{/}g;

    $self->debug("Probing UICL Interface package ".$self->{package}." from '$uicl_file'");
    eval {
        require $uicl_file;
    };
    if ($@) {
        $self->register_error("Could not load UICL Interface package '".$self->{package}."': $@");
        return 0;
    }

    $self->debug("Instanciating UICL package");
    eval {
        $self->{uicl} = $self->{package}->new( {
            test_uicl   => $self->{test_uicl},
        } );
    };
    if ($@) {
        $self->register_error("Could not instanciate UICL Interface package: $@");
        return 0;
    }

    $self->debug("Configuring UICL package");
    if (!$self->{uicl}->configure()) {
        $self->register_error("Configuration failed: ".$self->{uicl}->get_error());
        return 0;
    }

    $self->debug("Initializing UICL package");
    if (!$self->{uicl}->initialize()) {
        $self->register_error("Initialization failed: ".$self->{uicl}->get_error());
        return 0;
    }

    if (!$self->{test_uicl}) {
        $self->debug("Omitting UICL package test");
    } else {
        $self->debug("Testing UICL Interface package");
        if (!$self->{uicl}->test()) {
            $self->register_error("Testing failed: ".$self->{uicl}->get_error());
            return 0;
        }
    }

    $self->debug("UICL Interface ready");
    return 1;
}


=over 4

=item C<AUTOLOAD>

=back

=cut
sub AUTOLOAD {
    our $AUTOLOAD;
    my ($self) = shift;
    my ($args) = @_;

    my $uicl_method = ($AUTOLOAD =~ m{([^:]+$)})[0];

    my $result = undef;
    $result = $self->{uicl}->$uicl_method($args);
    $self->register_error($self->{uicl}->get_error()) if ($self->{uicl}->has_error());
    return $result;
}


=pod Destructor method (needed by AUTOLOAD)
=cut
sub DESTROY {
    my ($self) = shift;
    undef $self;
}

1;
