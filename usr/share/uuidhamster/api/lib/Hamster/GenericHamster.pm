package Hamster::GenericHamster;

=head1 NAME

Hamster::GenericHamster


=head1 SYNOPSIS

  use Hamster::GenericHamster;

  my $hamster = Hamster::GenericHamster->new( {
      ident       => "212.226.35.23",
      backend     => "Hamster::Backend::MyOwnBackend",
      driver      => "Hamster::Driver::Temp",
      autoinit    => 0,
    );

  print $hamster->get();


=head1 DESCRIPTION

This is a generic Hamster package. It does not have any Adapters or Backends by default. You may want to use it with a specific backend and/or driver package to have this Hamster as an anonymous and/or temporary autonomous data storage.

Note, that a backend must be specified that must provide a C<fill_cheek()> method.

If you don't specify a Backend, make sure to a) not autoinitialize and b) reset errors after the first get() operation on this Hamster.

=cut

use strict; use warnings;
use Hamster::Base;
use base qw(Hamster::Base);


sub getVersion {
    return "1.0";
}

sub init {
    my ($self) = shift;
    $self->{autoinit} = 0;
    return 1;
}

=head1 DEPENDENCIES

This package does not have any dependencies.


=head1 AUTHOR

Tristan Cebulla <tristan.cebulla@1und1.de>, 1&1 Internet AG


=head1 BUGS

There are no known bugs.


=head1 COPYRIGHT & LICENSE

Copyright (c) 2011 Tristan Cebulla (<tristan.cebulla@1und1.de>). All rights reserved.

This module is free software; you can redistribute it and/or modify it under the same terms as
Perl itself. See L<perlartistic>.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=cut

1;
