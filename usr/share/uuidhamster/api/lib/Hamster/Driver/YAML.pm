package Hamster::Driver::YAML;

=head1 NAME

Hamster::Driver::YAML


=head1 SYNOPSIS

=over 2

use Hamster::Driver::YAML;

my $driver = Hamster::Driver::YAML->new( {
    hamster     => "Hamster::TestHamster",
    lifetime    => 86400,
} );

=back


=head1 DESCRIPTION

This is the YAML storage driver for the Hamster driver storage interface. It uses YAML or YAML::Syck as storage backend.


=head1 DEPENDENCIES 

YAML
Sys::Hostname


=head1 DATA CHEEK FILE

This Hamster saves all data in a YAMLed cheek file. This file resides on different locations, depending on the Hamsters name (note that all Hamsters carry a suffix 'V3') and the machine name where the Hamster is running.

=over 4

=item Hamster::PatternHamsterV3

On infoNGs/RTRs the cheek file location and name is /kunden/tmp/abuse/patternhamster.hv3, on all other hosts /tmp/patternhamster.hv3.

=item Hamster::GeoIPHamsterV3

On infoNGs/RTRs the cheek file location and name is /kunden/tmp/abuse/geoiphamster.hv3, on all other hosts /tmp/geoiphamster.hv3.

=item Hamster::UberHamsterV3

On infoNGs/RTRs the cheek file location and name is /kunden/tmp/abuse/uberhamster.hv3, on all other hosts /tmp/uberhamster.hv3.

=item Other Hamsters

If a Hamsters name is not specified in here the default location is /tmp/Hamster_UnknownHamster.hv3 for non-infoNG/RTR Hamsters or /kunden/tmp/abuse/Hamster_UnknownHamster.hv3.

=back

=head1 ERROR HANDLING

This package uses L<BaseClass> for error reporting and handling. If an error happened you may check $driver->has_error().
The error message itself can be obtained from $driver->get_error().

For more information read the L<BaseClass documentation|BaseClass>.

=cut

use strict; use warnings;
use Hamster::Driver::Base;
use base qw(Hamster::Driver::Base);

use Sys::Hostname;
use Carp;


=head1 METHODS


=over 4

=item C<_init>

Probing YAML::Syck and YAML. Precedence has YAML::Syck because its faster that YAML.

=back

=cut
sub _init {
    my ($self) = shift;

    eval {
        use YAML;
    };
    if ($@) {
        my $message = "YAML is not available";
        $self->register_error($message);
        croak $message;
    }

    $YAML::UseCode = 1;

    $self->{use_syck} = 0;
    $self->debug("Using YAML as backend mechanism");

    return 1;
}


=over 4

=item C<configure>

  if (!$driver->configure()) {
      print STDERR $driver->get_error();
  }

Determines the Hamsters cheek data file depending on the Hamsters location (machine) and the Hamsters package name.

=back

=cut
sub configure {
    my ($self, $ident) = @_;

    my $file_suffix = "hv3";
    my $public_hv3 = 0;

    my $hostname = hostname;
    my $base_path = "/tmp";
    if ($hostname =~ m{^(infong|icpu).*}) {
        $base_path = "/kunden/tmp/abuse";
    } else {
        $public_hv3 = 1;
    }

    # default file
    my $filename = $self->{hamster};
    $filename =~ s{::}{_};

    # special Hamster files
    if ($self->{hamster} eq "Hamster::GeoIPHamster") {
        $filename = "geoiphamster";
    } elsif ($self->{hamster} eq "Hamster::UberHamster") {
        $filename = "uberhamster";
    } elsif ($self->{hamster} eq "Hamster::PatternHamster") {
        $filename = "patternhamster";
    } elsif ($self->{hamster} eq "Hamster::MWSHamster") {
        $filename = (defined $ident) ? sprintf ("mwshamster_%s", $ident) : "mwshamster";
    }

    $self->{cheek_file} = $base_path."/".$filename.".".$file_suffix;
    $self->debug("Driver configured successfully with cheek data file '".$self->{cheek_file}."'");

    if ($^O eq "linux" && $public_hv3) {
        $self->debug("Setting cheekfile permissions to 'public'");
        use Util::ExecUtil;    # we use the ExecUtil here we don't know which YAML driver is taken
        my $util = Util::ExecUtil->new();

        if (! -f $self->{cheek_file}) {
            $self->debug("Cheekfile '".$self->{cheek_file}."' does not exist, creating an empty one");
            my $touch = $util->execute("touch ".$self->{cheek_file});
            if (!$touch->{success}) {
                $self->register_error("Could not create empty cheekfile: ".$touch->{stderr});
                $self->reset_error();
            }
        }

        my $chmod = $util->execute("chmod 0666 ".$self->{cheek_file});
        if (!$chmod->{success}) {
            $self->register_error("Could not set to public mode: ".$chmod->{stderr});
            $self->reset_error();
        }
    }

    return 1;
}


=over 4

=item C<_reload_cheek_data>

if (!$driver->_reload_cheek_data()) {
    print STDERR $driver->get_error();
}

Reload the cheek data in the cheek data file.

=back

=cut
sub _reload_cheek_data {
    my ($self) = shift;

    $self->debug("Reloading cheek data file");
    eval {
        if ($self->{use_syck}) {
            $self->{storage} = YAML::Syck::LoadFile($self->{cheek_file});
        } else {
            $self->{storage} = YAML::LoadFile($self->{cheek_file});
        }
    };
    if ($@) {
        $self->register_error("Reload failed: $@");
        return 0;
    }
    return 1;
}


=over 4

=item C<initialize>

$driver->initialize();

Intialize the YAML package and the cheek data with an empty hash if the cheek file does not exist. Otherwise the current YAML content is taken.

If the C<YAML::LoadFile()> returns an error an error message is available via C<$driver->get_error()>.

=back

=cut
sub initialize {
    my ($self) = shift;

    # try to load the cheek file.
    eval {
        if ($self->{use_syck}) {
            $self->{storage} = YAML::Syck::LoadFile($self->{cheek_file});
        } else {
            $self->{storage} = YAML::LoadFile($self->{cheek_file});
        }
    };

    # cheek file does not exists (or could not be loaded due to other errors like permissions, etc)
    if ($@) {
        $self->register_error("Could not load data cheek file");

        # try to inizialize an empty cheek file
        eval {
            if ($self->{use_syck}) {
                YAML::Syck::DumpFile($self->{cheek_file}, {});
            } else {
                YAML::DumpFile($self->{cheek_file}, {});
            }
        };
        # did not work, permanent failure
        if ($@) {
            $self->register_error("Could not create empty data cheek file: $@");
            return 0;
        }
    }

    $self->reset_error();
    if ($self->{use_syck}) {
        $self->debug("Driver initialized successfully with YAML::Syck");
    } else {
        $self->debug("Driver initialized successfully with YAML");
    }
    return 1;
}


=over 4

=item C<test>

$driver->test();

This method always returns true (1) because I<normal> operations on YAML data should always work properly.

=back

=cut
sub test {
    my ($self) = shift;
    $self->debug("Driver tested successfully");
    return 1;
}



=over 4

=item C<get>

my $data = $driver->get($ident);

Returns the corresponding data to a unique identifier in the current namespace out of the YAML file.

=over 4

=item C<$ident>

The identifier for this data cheek.

=back

=back

=cut
sub get {
    my ($self, $ident) = @_;

    # first try to get from temporary cache when the soft_set feature is enabled
	return $self->{temp}->{$ident} if (defined $self->{temp}->{$ident});

    # otherwise read directly from storage
    return $self->{storage}->{$ident}   if defined $self->{storage}->{$ident};
    return undef;
}


=over 4

=item C<set>

if (!$driver->set($ident, { })) {
    print STDERR $driver->get_error();
}

Saves data to the YAML file. Note that this driver does save the special Hamster data structure inside of the YAML file:

    {
        data            => $data,
        creation_date   => time(),
    }

Using this data structure follows the design pattern for cheek data.

=over 4

=item C<$ident>

The identifier for this data cheek

=item C<$data>

Any data structure containing data belonging to the identifier. You may save a scalar value, a hash or an array.

=back

=back

=cut
sub set {
    my ($self) = shift;
    my ($ident) = shift;

    ($self->{storage}->{$ident}) = @_;

    # save it to the yaml file
    eval {
        if ($self->{use_syck}) {
            YAML::Syck::DumpFile($self->{cheek_file}, $self->{storage});
        } else {
            YAML::DumpFile($self->{cheek_file}, $self->{storage});
        }
    };
    if ($@) {
        $self->register_error("Could not save cheek data for ident '$ident': $@ ($!)");
        return 0;
    }

    # reload cheek data
    return 0 if (!$self->_reload_cheek_data());
    return 1;
}


=over 4

=item C<update>

if (!$driver->update($ident, { })) {
    print STDERR $driver->get_error();
}

Updates some data in the YAML file. If $ident was not defined true (1) is returned anyway. If the C<delete()> or C<set()> methods return false (0) the correspoding error message can be retrieved via C<$driver->get_error()>. Otherwise true (1) is returned.

=over 4

=item C<$ident> (String)

The identifier for this dataset.

=item C<$data>

The data structure you want to set. You can save a scalar value, a hash or an array.

=back

=back

=cut
sub update {
    my ($self, $ident, $data) = @_;

    return 0 unless $self->delete($ident);
    return 0 unless $self->set($ident, $data);
    return 1;
}


=over 4

=item C<delete>

if (!$driver->delete($ident)) {
    print STDERR $driver->get_error();
}

Delete data for $ident in the YAML file.
current namespace. If $ident was not defined true (1) is returned anyway. Otherwise the return-value of the Cache::Memcached::delete() method is returned.

=over 4

=item C<$ident>

The identifier.

=back

=back

=cut
sub delete {
    my ($self, $ident) = @_;

    if (defined $self->{storage}->{$ident}) {
        delete $self->{storage}->{$ident};

        eval {
            if ($self->{use_syck}) {
                YAML::Syck::DumpFile($self->{cheek_file}, $self->{storage});
            } else {
                YAML::DumpFile($self->{cheek_file}, $self->{storage});
            }
        };
        if ($@) {
            $self->register_error($@);
            return 0;
        }
    }

    $self->_reload_cheek_data();
    return 1;
}


=item C<commit>

if (!$driver->commit()) {
    print STDERR $driver->get_error();
}

Commit changes that have been set with C<soft_set()>. If an error occurs the error message can be obtained from C<$driver->get_error()>. In that case false (0) is returned, otherwise true (1).

This driver keeps old data (set with C<set()>) in the cheek data structure.

=back

=cut
sub commit {
    my ($self) = shift;

    # read previous contents that might exist
    $self->initialize();

    foreach my $ident (keys %{$self->{temp}}) {
        # save to the storage hash
        $self->{storage}->{$ident} = {
            data            => $self->{temp}->{$ident}->{data},
            creation_date   => time(),
        };
        delete $self->{temp}->{$ident};
    }

    eval {
        if ($self->{use_syck}) {
            YAML::Syck::DumpFile($self->{cheek_file}, $self->{storage});
        } else {
            YAML::DumpFile($self->{cheek_file}, $self->{storage});
        }
    };
    if ($@) {
        $self->register_error("Error while committing changes: $@");
        return 0;
    }

    $self->{temp} = {};
    $self->_reload_cheek_data();
    return 1;
}


=over 4

=item C<flush>

Flushes/invalidates all data in the YAML file.

=back

=cut
sub flush {
    my ($self) = shift;
    $self->{storage} = {};

    eval {
        if ($self->{use_syck}) {
            YAML::Syck::DumpFile($self->{cheek_file}, $self->{storage});
        } else {
            YAML::DumpFile($self->{cheek_file}, $self->{storage});
        }
    };
    if ($@) {
        $self->register_error($@);
        return 0;
    }
    return 1;
}


sub DESTROY {
    my ($self) = @_;
	$self->commit() if (scalar keys %{$self->{temp}} != 0);
}


=head1 AUTHOR

Tristan Cebulla <tristan.cebulla@1und1.de>, 1&1 Internet AG


=head1 BUGS

There are no known bugs.


=head1 COPYRIGHT & LICENSE

Copyright (c) 2011 Tristan Cebulla (<tristan.cebulla@1und1.de>). All rights reserved.

This module is free software; you can redistribute it and/or modify it under the same terms as
Perl itself. See L<perlartistic>.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=cut

1;
