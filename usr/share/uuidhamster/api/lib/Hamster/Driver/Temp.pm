package Hamster::Driver::Temp;

=head1 NAME

Hamster::Driver::Temp


=head1 SYNOPSIS

  use Hamster::Driver::Temp;

  my $driver = Hamster::Driver::Temp->new( {
      hamster     => "Hamster::TestHamster",
      lifetime    => 86400,
  } );


=head1 DESCRIPTION

This is the temporary data storage driver for the Hamster driver storage interface. It uses a hash for storing information. Note that this Hamster will loose all information when it's object is destroyed.


=head1 DEPENDENCIES 

This package has no dependencies.


=head1 ERROR HANDLING

This package uses L<BaseClass> for error reporting and handling. If an error happened you may check $driver->has_error().
The error message itself can be obtained from $driver->get_error().

For more information read the L<BaseClass documentation|BaseClass>.

=cut


use strict; use warnings;
use Hamster::Driver::Base;
use base qw(Hamster::Driver::Base);


=head1 METHODS

=over 4

=item C<initialize>

$driver->initialize();

This method always return true (1) since hash operations should always succeed.

=back

=cut
sub initialize {
    my ($self) = shift;
    $self->debug("Driver initialized successfully");
    return 1;
}


=over 4

=item C<test>

$driver->test();

This method always return true (1) since hash operations should always succeed.

=back

=cut
sub test {
    my ($self) = shift;
    $self->debug("Driver tested successfully");
    return 1;
}


=over 4

=item C<get>

my $data = $driver->get($ident);

Returns the corresponding data to a unique identifier from the storage.

=over 4

=item C<$ident>

The identifier for this data cheek.

=back

=back

=cut
sub get {
    my ($self) = shift;
    my ($ident) = shift;

    return $self->{storage}->{$ident} || undef;
}


=over 4

=item C<set>

$driver->set($ident, { });

Saves data to the hash storage. Note that this driver does save the special Hamster data structure inside of hash data structure:

    {
        data            => $data,
        creation_date   => time(),
    }

Using this data structure follows the design pattern for cheek data. This method always returns true (1).

=over 4

=item C<$ident>

The identifier for this data cheek

=item C<$data>

Any data structure containing data belonging to the identifier. You may save a scalar value, a hash or an array.

=back

=back

=cut
sub set {
    my ($self) = shift;
    my ($ident) = shift;

    ($self->{storage}->{$ident}) = @_;
    return 1;
}


=over 4

=item C<delete>

$driver->delete($ident);

Delete data for $ident in the hash storage. This method always returns true (1).

=over 4

=item C<$ident>

The identifier.

=back

=back

=cut
sub delete {
    my ($self) = shift;
    my ($ident) = shift;

    undef $self->{storage}->{$ident};
    delete $self->{storage}->{$ident};
    return 1;
}


=over 4

=item C<update>

$driver->update($ident, { });

Updates some data in the hash storage. This method always returns true (1).

=over 4

=item C<$ident> (String)

The identifier for this dataset.

=item C<$data>

The data structure you want to set. You can save a scalar value, a hash or an array.

=back

=back

=cut
sub update {
    my ($self) = shift;
    my ($ident, $data) = @_;

    $self->delete($ident);
    $self->set($ident, $data);
    return 1;
}


=over 4

=item C<commit>

if (!$driver->commit()) {
    print STDERR $driver->get_error();
}

Commit changes that have been set with C<soft_set()>. If an error occurs the error message can be obtained from C<$driver->get_error()>. Note that this driver just copies all data from C<$self->{tmp}> to C<$self->{storage}> (which means that previous data will get lost) so it will always return true (1).

=back

=cut
sub commit {
    my ($self) = shift;

    $self->{storage} = $self->{tmp};
    $self->{tmp} = {};
    return 1;
}


=over 4

=item C<flush>

Flushes/invalidates all data in the hash storage.

=back

=cut
sub flush {
    my ($self) = shift;
    $self->{storage} = {};
    return 1;
}


=head1 AUTHOR

Tristan Cebulla <tristan.cebulla@1und1.de>, 1&1 Internet AG


=head1 BUGS

There are no known bugs.


=head1 COPYRIGHT & LICENSE

Copyright (c) 2011 Tristan Cebulla (<tristan.cebulla@1und1.de>). All rights reserved.

This module is free software; you can redistribute it and/or modify it under the same terms as
Perl itself. See L<perlartistic>.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=cut

1;
