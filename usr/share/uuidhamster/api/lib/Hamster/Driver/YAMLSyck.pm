package Hamster::Driver::YAMLSyck;

=head1 NAME

Hamster::Driver::YAMLSyck

See Hamster::Driver::YAML for documentation.

=cut

use strict; use warnings;
use Hamster::Driver::YAML;
use base qw(Hamster::Driver::YAML);

use Sys::Hostname;
use Carp;


=head1 METHODS


=over 4

=item C<_init>

Probing YAML::Syck

=back

=cut
sub _init {
    my ($self) = shift;

    # try YAML::Syck
    eval {
        use YAML::Syck;
    };
    # failed, try YAML
    if ($@) {
        my $message = "YAML::Syck is not available";
        $self->register_error($message);
        croak $message;
    }

    $YAML::Syck::UseCode = 1;

    $self->{use_syck} = 1;
    $self->debug("Using YAML::Syck as backend mechanism");

    return 1;
}

1;
