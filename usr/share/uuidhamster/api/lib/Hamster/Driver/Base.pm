package Hamster::Driver::Base;

=head1 NAME

Hamster::Driver::Base


=head1 SYNOPSIS

This package is not intented for being used directly. Use it as a base class for all Hamster drivers.

  package Hamster::Driver::MyDriver;

  use Hamster::Driver::Base;
  use base qw(Hamster::Driver::Base);


=head1 DESCRIPTION

This package provides default methods for every Hamster driver. The driver can be configured via the C<configure> method. Thereafter the driver will be initialized and tested via the C<initialize()> and C<test()> methods. All these methods must return true (1) in order to use that driver package.

Also this package includes the C<soft_set()> and C<commit> functionality. The extending driver package needs to implement it's own C<commit> functionality.


=head1 DEPENDENCIES 

This package does not have real dependencies since it only provides an interface for a driver package. The dependencies are specified in the driver package itself.


=head1 ERROR HANDLING

This package uses L<BaseClass> for error reporting and handling. If an error happened you may check $driver->has_error().
The error message itself can be obtained from $driver->get_error().

For more information read the L<BaseClass documentation|BaseClass>.

=cut

use strict; use warnings;
use BaseClass;
use base qw(BaseClass);


=head1 CONSTRUCTOR

=over 4

=item C<new>

The constructor takes a hashref as parameter containing these data:

=over 4

=item C<hamster>

The Hamsters package name

=item C<lifetime>

The cheek lifetime in seconds

=back

=back

=cut

sub new {
    my ($class) = shift;
    my ($args) = @_;

    my ($self) = bless {
        name        => $class,              # package name
        hamster     => $args->{hamster},    # Hamster package name
        lifetime    => $args->{lifetime},   # cheek data lifetime
        storage     => undef,               # storage location
        temp        => {},                  # temporary cheek data cache for soft_set/commit functionality
        DEBUG       => $args->{DEBUG},
    }, $class;

    # call private initialize method if the package supports that
    if ($self->can("_init")) {
        $self->debug("Calling _init()");
        $self->_init();
    }

    return $self;
}


=head1 METHODS

=over 4

=item C<get_driver_name>

Return the driver name of the current driver package

=back

=cut
sub get_driver_name {
    my ($self) = shift;
    return $self->{name};
}



=over 4

=item C<configure>

Default configuration method. Every driver package may overwrite this method to configure the driver. This method must return true (1) if the configuration succeeded, false (0) otherwise.

=back

=cut
sub configure {
    my ($self) = shift;
    $self->debug("Driver needs no configuration");
    return 1;
}


=over 4

=item C<initialize>

Virtual initialize method. This method dies because it must be overwritten from a storage child package.

=back

=cut
sub initialize {
    my ($self) = shift;
    die ("Driver package ".$self->{name}." needs it's own initialize method");
}


=over 4

=item C<test>

Virtual test method. This method dies because it must be overwritten from a storage child package.

=back

=cut
sub test {
    my ($self) = shift;
    die ("Driver package ".$self->{name}." needs it's own test method");
}


=over 4

=item C<soft_set>

$driver->soft_set($ident, $data);

=over 4

=item C<$ident>

The identifier for this data set.

=item C<$data>

Any data structure belonging to the identifier you want to save

=back

The I<soft_set> feature allows you to bulk-insert a set of data (identifier => data) at once. This will reduce time and load when saving tons of data, because the driver does not need to wait until a single dataset is stored. 

=back

=cut
sub soft_set {
    my ($self, $ident, $data) = @_;

    $self->{temp}->{$ident} = $data;

    # for some reason non-scalars/-hashes/-arrays do not survive the global destruction phase on auto-committing when called from DESTROY.
    # In this case we need to commit immediately which slows down the soft_set feature
    if (ref $data->{data} ne "HASH" && ref $data->{data} ne "ARRAY" && ref $data->{data} ne "") {
	return $self->commit();
    }
    return 1;
}


=over 4

=item C<commit>

Virtual commit method. This method dies because it must be overwritten from a storage child package.

=back

=cut
sub commit {
    my ($self) = shift;
    die ("Driver package ".$self->{name}." needs it's own commit method");
}


=head1 AUTHOR

Tristan Cebulla <tristan.cebulla@1und1.de>, 1&1 Internet AG


=head1 BUGS

There are no known bugs.


=head1 COPYRIGHT & LICENSE

Copyright (c) 2011 Tristan Cebulla (<tristan.cebulla@1und1.de>). All rights reserved.

This module is free software; you can redistribute it and/or modify it under the same terms as
Perl itself. See L<perlartistic>.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=cut

1;
