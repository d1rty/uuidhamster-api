package Hamster::Driver::Memcached;

=head1 NAME

Hamster::Driver::Memcached


=head1 SYNOPSIS

=over 2

use Hamster::Driver::Memcached;

my $driver = Hamster::Driver::Memcached->new( {
    hamster     => "Hamster::TestHamster",
    lifetime    => 86400,
} );

=back


=head1 DESCRIPTION

This is the memory caching daemon (memcached) driver for the Hamster driver storage interface. It uses Cache::Memcached as storage backend.


=head1 DEPENDENCIES 

Cache::Memcached


=head1 ERROR HANDLING

This package uses L<BaseClass> for error reporting and handling. If an error happened you may check $driver->has_error().
The error message itself can be obtained from $driver->get_error().

For more information read the L<BaseClass documentation|BaseClass>.

=cut

use strict; use warnings;
use Hamster::Driver::Base;
use base qw(Hamster::Driver::Base);

use Cache::Memcached;
use YAML;

=head1 DEFAULT MEMCACHED HOSTS

We use the memcache daemons on

=over 2

=item 10.1.73.213:11211

abu-gugg.v300.gmx.net

=item 172.19.17.183:11211

abuhome.schlund.de

=back

These hosts are configured in /etc/hamster/driver/memcached.conf


=head1 METHODS

=over 4

=item C<configure>

  if (!$driver->configure()) {
      print STDERR $driver->get_error();
  }

Loads the memcached hosts from the configuration file. If this does not succeed, an error message is available.

=back

=cut
sub configure {
    my ($self) = @_;

    my $available_config_files;

    # fix to avoid error messages in scripts where $ENV{HOME} is unset (like in the postfix|pipe environment)
    {
        no warnings 'uninitialized';
        if ($^O eq "linux") {
            $available_config_files = [
                "/etc/hamster/driver/memcached.conf",
                $ENV{HOME}."/.hamsterrc/driver/memcached.conf",
            ];
        } elsif ($^O eq "windows") {
            $available_config_files = [
                "C:\\Temp\\driver\\memcached.conf",
            ];
        }
    };

    while (!defined($self->{memcached_hosts}) and scalar @$available_config_files) {
        my $current_config_file = shift @$available_config_files;
        eval {
            $self->{memcached_hosts} = YAML::LoadFile($current_config_file);
        }; if ($@) {
            $self->register_error("Could not load configuration file: $current_config_file, error was: $@");
        } else {
            $self->reset_error(); # in case an error happened before
            $self->debug("Driver configured successfully with configuration file '$current_config_file'");
            return 1;
        }
    }

    $self->register_error("Could not load configuration file: $@");
    return 0;
}

=over 4

=item C<initialize>

  $driver->initialize();

Intialize the Cache::Memcached package. As memcached namespace the Hamsters package name is used. Compression is enabled by default. This method always returns true (1) since Cache::Memcached does not report any errors when it gets instanced.

=back

=cut
sub initialize {
    my ($self) = shift;

    $self->{namespace} = sprintf("%s|", $self->{hamster});

    $self->{storage} = new Cache::Memcached(
        servers                 => $self->{memcached_hosts},
        debug                   => 0,
        compress_threshold      => 10_000,
        namespace               => $self->{namespace},
    );

    $self->{storage}->enable_compress(1);

    $self->debug("Driver initialized successfully with Cache::Memcached");
    return 1;
}


=over 4

=item C<test>

  if (!$driver->test()) {
      print STDERR $driver->get_error();
  }

Test the Cache::Memcached set, get and delete operations. If all succeed true (1) is returned, otherwise false (0).

If an error occurs the error message can be obtained from the get_error() method in the driver interface.

=back

=cut
sub test {
    my ($self) = @_;

    # generate a random string
    my $test_string;
    my @chars = ('a'..'z','A'..'Z','0'..'9','_');
    $test_string .= $chars[rand @chars] foreach (1..20);

    if (!$self->{storage}->set($self->{name}, $test_string, 5)) {
        $self->register_error("Test failed: Could not store test data");
        return 0;
    }

    if ($test_string ne $self->{storage}->get($self->{name})) {
        $self->register_error("Test failed: Test data result does not match predefined value");
        return 0;
    }

    if (!$self->{storage}->delete($self->{name})) {
        $self->register_error("Could not delete test data");
        return 0;
    }

    $self->debug("Driver tested successfully");
    return 1;
}


=over 4

=item C<get>

  my $data = $driver->get($ident);

Returns the corresponding data to a unique identifier in the current namespace out of memcached.

=over 4

=item C<$ident>

The identifier for this data cheek.

=back

=back

=cut
sub get {
    my ($self, $ident) = @_;
    $self->debug(sprintf("Returning data for ident %s%s", $self->{namespace}, $ident));
    return $self->{storage}->get($ident);
}


=over 4

=item C<set>

  if (!$driver->set($ident, { })) {
      print STDERR $driver->get_error();
  }

Saves data to memcached.

Note that the incoming data are preprocessed by the C<hamsterify_cheek_data()> method. This is not implicitly necessary since the set-method supports a third parameter that specifies the lifetime of a dataset. But using this data structure follows the design pattern for cheek data, and it does not really cost much memory.

=over 4

=item C<$ident>

The identifier for this data cheek

=item C<$data>

Any data structure containing data belonging to the identifier. You may save a scalar value, a hash or an array.

=back

=back

=cut
sub set {
    my ($self, $ident, $data) = @_;
    return $self->{storage}->set($ident, $data, $self->{lifetime});
}


=over 4

=item C<update>

  if (!$driver->update($ident, { })) {
      print STDERR $driver->get_error();
  }

Updates some data in memcached. If $ident was not defined true (1) is returned anyway. Note that the Cache::Memcached::replace() method is NOT used, since it would return false (0) if a key does not exist in the current namespace.

=over 4

=item C<$ident> (String)

The identifier for this dataset.

=item C<$data>

The data structure you want to set. You can save a scalar value, a hash or an array.

=back

=back

=cut
sub update {
    my ($self, $ident) = @_;

    if (!$self->delete($ident)) {
        $self->register_error("Ident '$ident' could not be deleted because it did not exist.");
    }

    return $self->set($ident, @_, $self->{lifetime});
}


=over 4

=item C<delete>

  if (!$driver->delete($ident)) {
      print STDERR $driver->get_error();
  }

Delete data for $ident in the current namespace. If $ident was not defined true (1) is returned anyway. Otherwise the return-value of the Cache::Memcached::delete() method is returned.

=over 4

=item C<$ident>

The identifier.

=back

=back

=cut
sub delete {
    my ($self) = shift;
    my ($ident) = shift;
    return 1 unless defined $ident;
    return ($self->{storage}->delete($ident)) ? 1 : $self->register_error("Could not delete");
}


=over 4

=item C<commit>

  if (!$driver->commit()) {
      print STDERR $driver->get_error();
  }

Commit changes that have been set with C<soft_set()>. If an error occurs the error message can be obtained from C<$driver->get_error()>. In that case false (0) is returned, otherwise true (1).

=back

=cut
sub commit {
    my ($self) = shift;

    foreach my $ident (keys %{$self->{temp}}) {
        if (!$self->set($ident, $self->{temp}->{$ident})) {
            $self->register_error("Error while committing ident '$ident': ".$self->get_error());
            return 0;
        }
        delete $self->{temp}->{$ident};
    }

    $self->{temp} = {};
    return 1;
}


=over 4

=item C<flush>

  $driver->flush();

Flushes/invalidates all caches in all namespaces on all memcached-hosts.

=back

=cut
sub flush {
    my ($self) = shift;
    $self->{storage}->flush_all();
    return 1;
}


=head1 FILES

  /etc/hamster/driver/memcached.conf


=head1 AUTHOR

Tristan Cebulla <tristan.cebulla@1und1.de>, 1&1 Internet AG


=head1 BUGS

There are no known bugs.


=head1 COPYRIGHT & LICENSE

Copyright (c) 2011 Tristan Cebulla (<tristan.cebulla@1und1.de>). All rights reserved.

This module is free software; you can redistribute it and/or modify it under the same terms as
Perl itself. See L<perlartistic>.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=cut

1;
