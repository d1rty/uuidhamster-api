package Hamster::Driver::YAMLXS;

=head1 NAME

Hamster::Driver::YAMLXS

See Hamster::Driver::YAML for documentation.

=cut

use strict; use warnings;
use Hamster::Driver::YAML;
use base qw(Hamster::Driver::YAML);

use Sys::Hostname;
use Carp;


=head1 METHODS


=over 4

=item C<_init>

Probing YAML::XS

=back

=cut
sub _init {
    my ($self) = shift;

    # try YAML::Syck
    eval {
        use YAML::XS;
    };
    # failed, try YAML
    if ($@) {
        my $message = "YAML::XS is not available";
        $self->register_error($message);
        croak $message;
    }

    $YAML::XS::UseCode = 1;

    $self->debug("Using YAML::XS as backend mechanism");

    return 1;
}

1;
