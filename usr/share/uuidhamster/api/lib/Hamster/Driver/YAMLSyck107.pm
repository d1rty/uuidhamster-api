package Hamster::Driver::YAMLSyck107;

=head1 NAME

Hamster::Driver::YAMLSyck107

See Hamster::Driver::YAML for documentation.

=cut

use strict; use warnings;
use Hamster::Driver::YAML;
use base qw(Hamster::Driver::YAML);

use Sys::Hostname;
use Carp;


=head1 METHODS


=over 4

=item C<_init>

Probing YAML::Syck version 1.07

=back

=cut
sub _init {
    my ($self) = shift;

    eval {
        use YAML::Syck;
    };

    if ($@) {
        my $message = "YAML::Syck is not available";
        $self->register_error($message);
        croak $message;
    }

    if ( ( !defined $YAML::Syck::VERSION ) || ( $YAML::Syck::VERSION lt 1.07 ) ) {
        my $message = "At least YAML::Syck version 1.07 is required to work properly";
        $self->register_error($message);
        croak $message;
    }

    $YAML::Syck::UseCode = 1;

    $self->{use_syck} = 1;
    $self->debug("Using YAML::Syck as backend mechanism");

    return 1;
}

1;
