package Hamster::Base;


=head1 NAME

Hamster::Base


=head1 SYNOPSIS

  use Hamster::TestHamster;

There are two ways to instanciate a Hamster package:

=over 2

Instanciate the Hamster the old-school way with the first parameter being the identifier for the current cheek you want to fill:

  my $hamster = Hamster::TestHamster->new($ident);

Instanciate the Hamster with a hashref like so:

  my $hamster = Hamster::TestHamster->new(
      ident         => "some-identifier",
      driver        => "Hamster::Driver::MyDriver",
      backend       => "Hamster::Backend::MyBackend",
      soft_set      => 0,
      alias         => "HamsterAlias,
      autoinit      => 0,
  );


=over 2

=item C<ident>

The identifier for the current cheek you want to fill

=item C<driver>

The driver you want to use explicitly

=item C<backend>

The backend you want to use explicitly

=item C<alias>

The Hamsters alias name (useful for the GenericHamster)

=item C<soft_set>

Whether you want to use the drivers soft_set/commit methods. This is some kind of bulk-insert.

=item C<autoinit>

Automatically initializes the Hamster via the identifier. If unset (which means a default true value) and an identifier was given, the Hamster does initializes itself by default.

=back

=back


=head1 DESCRIPTION

This package provides default methods for every Hamster. This includes:

    performing read and write operations on the interface storage driver
    extending the Hamsters functionality with Adapters
    providing a unique data read and write-management
    backend functionality for how to get the requested data

=head2 Instancing and initializing the storage interface

After instanciating the Hamster object, the storage interface gets in action. This means to instanciate the interface which would try a series of drivers. If that fails an error message is available.

=head2 Intializing and testing the storage driver package

After the interface decided which driver to use, the driver itself will be initialized and tested. These methods (C<initialize()> and C<test()>) must return true (1) if they worked properly, false (0) otherwise. In that case an error message is available.

=head2 Instancing and initializing the backend interface

FIXME

=head2 Initializing and testing the backend package

FIXME

=head2 Initializing the Hamster package

The Hamster package itself supports an C<init()> method which is optional to have. This method needs to return true (1) if the initialization process was successful, false (0) if not. An error message is available in that case.

=head2 Initializing the Hamsters Adapter package

If the Hamster package does support an Adapter package (determined by returning a scalar string by C<hasAdapter()>) this package is instanciated. Accessing methods on the Adapter is as easy as accessing methods of the Hamster package, thanks to the C<AUTOLOAD> features. See the next chapter for more information on this feature.


=head1 DATA BACKEND INTERFACE

FIXME


=head1 DATA STORAGE INTERFACE

FIXME


=head1 ADAPTERS

Adapters extends the Hamsters functionality. Since the Hamster package I<should> only contain the C<init()>, C<fill_cheek()> and C<preprocess_ident()> methods, you may want to have more functionality. To enable an adapter simply add a method C<hasAdapter()> which only returns the perl package name of the adapter:

  sub hasAdapter {
      return "Hamster::Adapter::TestHamsterAdapter";
  }

The adapter package must use Hamster::Adapter::Base as a base package in order to work properly with the adapter caching system. Then you can start to specify your methods in the adapter package like so:

  sub get_uppercase {
      my ($ident) = shift;
      $ident ||= $self->{ident};

      # more args must be passed as hashref:
      my ($data) = @_;  

      return uc($ident);
  }
  
Then just call this method treating it as a method of the Hamster package. The AUTOLOAD functionality calls this method out of the adapter package:

  print $hamster->get_uppercase($ident);
  print $hamster->get_uppercase();
  

Every time some data have been written or read via the storage interface, the adapters cache is updated with these data to have these present in C<$self->{cache}> in the adapter package. See the Hamster::AdapterBase package documentation for more information on how this works.

Please note, that you B<must not> call any adapter method in the C<fill_cheek()> method. This would result in a permanent C<get()> method-loop, because the adapters cache needs to be initialized with cheek data before any adapter method can be called. 


=head1 HAMSTER CHEEKS

FIXME


=head2 HAMSTER CHEEK DATA STRUCTURE

The Hamsters cheek data have an own hashref data structure that B<all> drivers B<must> follow. This structure saves the cheek data in the hash key I<data>. The hash key I<creation_date> saves a timestamp using I<time()> with the creation date of the cheek:

  my $cheek_data = {
      data            => undef,
      creation_date   => time(),
  };

To provide this unique data format the C<hamsterify_cheek_data()> method must be used to save data to the storage engine. This method is available in the Hamster::Driver::Base package.

When returning data from a Hamsters cheek, the I<data>-key value is returned. This value may contain any data types: hashref, arrayref or scalar.

Please note, that this is not necessary for the memcached storage driver because C<Cache::Memcached> manages the lifetime itself. However, this driver saves this data structure, too.


=head2 HAMSTER CHEEK DATA EXPIRATION

Each Hamsters cheek data have a lifetime, the default lifetime is 86400 seconds (24 hours). If you want to modify this lifetime you would do best setting this lifetime in the Hamsters C<init()> method. To check if the Hamsters cheek data have expired, the C<is_lifetime_expired($creation_date)> method is used.

  my $cheek_data;

  if ($self->is_lifetime_expired($creation_date)) {
      $cheek_data = $self->_update_cheek_data($ident);
  }


=head1 PREPROCESSING THE IDENTIFIER

If the C<_preprocess_ident()> method is available in the Hamster package the methods return value is taken as identifier. The method takes the current $ident variable as first parameter. You may do whatever you want in there.

Following best-practices when using this feature along with the Backend feature, you may want to access the backend methods via $self->{backend}->method() in the C<_preprocess_ident()> method.


=cut


use strict; use warnings;
use BaseClass;
use base qw(BaseClass);

use Hamster::Interface::Driver;
use Hamster::Interface::Backend;
use Carp;


=head1 VERSION

This is the Hamsters base, major version 3

=cut
my $major   = 3;
my $devel   = 6;
my $minor   = 0;


=head1 METHODS

=over 4

=item C<new>

Create a new instance of this package. The constructor supports the following hash-args:

=over 4

=item C<ident>

The identifier that runs the Hamster package. This parameter is optional.

=item C<driver> (String)

The storage driver name (e.g. Hamster::Driver::MyOwnDriver) to use explicitly. No other drivers are probed. This parameter is optional.

=item C<backend>

The backend package (e.g. Hamster::Backend::MyOwnBackend) to use explicitly. No other backends are probed, unless the Hamster package does support other backend packages. This paramter is optional.

=item C<alias>

The Hamsters alias name. This is useful when using the GenericHamster to give him a more meaningful name. This parameter is optional.

=item C<lifetime> (Integer)

The cheek lifetime. It defaults to 86400 seconds. This parameter is optional.

=item C<soft_set> (Boolean)

Use the soft_set/commit methods. This can be useful when using the Hamster::Driver::YAML driver to create auto-references by the YAML engine.
This means that all entries are written at once and not sequentially. This parameter is optional.

=back

=back

=cut
sub new {
    my ($class, $args) = @_;

    # transform args to hashref
    $args = { ident => $args } unless ref $args eq "HASH";

    my $self = bless {
        name        => $args->{alias} || $class,
        ident       => $args->{ident},
        autoinit    => $args->{autoinit}    || 1,
        pkg_test    => $args->{pkg_test}    || 0,
        driver      => undef,
        backend     => undef,
        adapter     => undef,
    }, $class;

    $self->debug(sprintf("Powering up %s", $self->{name}));

    # initialize storage interface and throw error if the initialization did not work properly
    confess($self->get_error()) unless $self->_init_driver( {
        hamster     => $self->{name},
        driver      => $args->{driver}      || undef,
        soft_set    => $args->{soft_set}    || 0,
        lifetime    => $args->{lifetime}    || undef,
    } );

    # initialize backend and throw error if the initialization did not work properly
    confess($self->get_error()) unless $self->_init_backend( {
        hamster     => $self->{name},
        backend     => $args->{backend}     || undef,
    } );

    # initialize adapter and throw error if the initialization did not work properly
    if ($self->can("getAdapter")) {
        confess($self->get_error()) unless $self->_init_adapter($self->getAdapter());
    }

    # initialize package and throw error if the initialization did not work properly
    if ($self->can("init")) {
        $self->debug(sprintf("Calling %s::init()", $self->{name}));
        confess($self->get_error()) unless $self->init();
    }

    # preprocess ident to another value if the Hamster has the _preprocess_ident() method
    if ($self->can("_preprocess_ident")) {
        $self->debug(sprintf("_preprocess_ident('%s') called", $self->{ident}));
        my $pident = $self->_preprocess_ident($self->{ident});

        confess($self->get_error()) if $self->has_error();
        confess("Could not preprocess ident, returned an undef value") unless defined $pident;

        $self->{ident} = $pident;
        $self->debug(sprintf("Preprocessed ident to '%s'", $self->{ident}));
    }

    # dump the current hamster configuration
    $self->dumpHamsterConfiguration();

    # auto-initialize the Hamster
    if ($self->{autoinit} && defined $self->{ident} && $self->{ident} ne "") {
        $self->debug(sprintf("Autoinitializing %s with ident '%s'", $self->{name}, $self->{ident}));
        $self->get($self->{ident});
    }

    return $self;
}


=over 4

=item C<getBaseVersion>

Returns the Hamster::Base version

=back

=cut
sub getBaseVersion {
    return sprintf("%d.%d.%d", $major, $devel, $minor);
}


=over 4

=item C<getVersion>

Returns the Hamster version - should be overwritten in the Hamster package directly

=back

=cut
sub getVersion {
    return "not defined";
}


=over 4

=item C<dumpHamsterConfiguration>

Dumps the current Hamster configuration (only available when the HV3DEBUG environment variable is set 

=back

=cut
sub dumpHamsterConfiguration {
    my ($self) = @_;

    # basic hamster stuff
    $self->debug(sprintf("%s is ready for action", $self->{name}));
    $self->debug(sprintf("  Hamster Base Version    : '%s'" ,$self->getBaseVersion()));
    $self->debug(sprintf("  Hamster Version         : '%s'", $self->getVersion()));
    $self->debug(sprintf("  Ident                   : '%s'", (defined $self->{ident}) ? $self->{ident} : "uninitialized"));
    $self->debug(sprintf("  Auto-initialize         : %s", ($self->{autoinit} == 1) ? "enabled" : "disabled"));
    $self->debug(sprintf("  Package testing         : %s", ($self->{pkg_test} == 1) ? "enabled" : "disabled"));

    # driver stuff
    $self->debug(sprintf("  Driver Interface        : %s", $self->{driver}->get_driver_name()));
    $self->debug(sprintf("    Cheek data lifetime   : '%s' seconds", $self->{driver}->get_lifetime()));
    $self->debug(sprintf("    soft-set Feature      : %s", ($self->{driver}->get_soft_set() == 1) ? "enabled" : "disabled"));

    # backend stuff
    my $backend_name        = $self->{backend}->get_backend_name();
    my $uicl_interface_name = "none";
    if (!defined $backend_name) {
        $backend_name           = "local fill_cheek() method";
    } else {
        $uicl_interface_name    = $self->{backend}->getUICL();
    }
    $self->debug(sprintf("  Backend Interface       : %s", $backend_name));
    $self->debug(sprintf("    UICL Interface        : %s", $uicl_interface_name));

    # adapter stuff
    my $adapter_name = (defined $self->{adapter}) ? $self->{adapter}->{name} : "disabled";
    $self->debug("  Adapter                 : ".$adapter_name);
}


=over 4

=item C<_init_driver>

Initialize the driver interface.

=back

=cut
sub _init_driver {
    my ($self, $args) = @_;

    # initialize with default values
    $args->{driver}     ||= undef;
    $args->{soft_set}   ||= 0;

    $self->debug("Initializing Driver Interface");

    $self->{driver} = Hamster::Interface::Driver->new( {
        hamster     => $self->{name},
        ident       => $self->{ident},
        driver      => $args->{driver},
        soft_set    => $args->{soft_set},
        lifetime    => $args->{lifetime},
        test_driver => $self->{pkg_test},
    } );

    if (!$self->{driver}->initialize()) {
        $self->register_error("Could not initialize storage driver: ".$self->{driver}->get_error());
        return 0;
    }

    return 1;
}


=over 4

=item C<force_driver>

  $hamster->force_driver($driver_package, $args);

Use a driver explicitly. This method is useful for calling it in the Hamsters C<init()> method if you want to force a Hamster to use a specific driver, whatever was passed with arguments at instanciation.

You may also specify an args-hashref with the cheek lifetime in seconds and whether to use the soft_set feature or not like so:

    {
      lifetime  => 86400,
      soft_set  => 1,
    }

=back

=cut
sub force_driver {
    my ($self, $driver, $args) = @_;

    $args->{lifetime}   ||= 86400;
    $args->{soft_set}   ||= 0;

    confess($self->get_error()) unless $self->_init_driver( {
        driver      => $driver,
        lifetime    => $args->{lifetime},
        soft_set    => $args->{soft_set},
    } );
    return 1;
}


=over 4

=item C<_init_backend>

Initialize the backend

=back
   
=cut
sub _init_backend {
    my ($self, $args) = @_;
    my @backends = ();

    # forced backend
    if (defined $args->{backend}) {
        @backends = ( $args->{backend} );

    # own backend package
    } elsif ($self->can("getBackends")) {
        @backends = ( $self->getBackends() );

    # default fill_cheek method from Hamster package
    } else {
        # leave @backends empty
    }

    # instanciate backend interface
    my $debug_backends = join(",", @backends);
    $debug_backends = "undef" if ($debug_backends eq "");
    $self->debug("Initializing Backend Interface with: '".$debug_backends."'");
    $self->{backend} = Hamster::Interface::Backend->new( {
        hamster         => $args->{hamster},
        backends        => [ @backends ],
        test_backend    => $self->{pkg_test},
    } );

    if (!$self->{backend}->initialize()) {
        $self->register_error($self->{backend}->get_error());
        return 0;
    }

    return 1;
}


=over 4

=item C<_init_adapter>

  if (!$hamster->_init_adapter($adapter_name)) {
      print STDERR $hamster->get_error();
  }

=over 4

=item C<$adapter>

The Adapters package name

=back

Initialize the Adapter package. If the adapter could not be initialized an error message will be present.

=back

=cut
sub _init_adapter {
    my ($self, $adapter) = @_;

    eval "require $adapter";
    if ($@) {
        $self->register_error("Could not load the adapter '$adapter': $@");
        return 0;
    }
    $self->{adapter} = $adapter->new( {
        driver          => $self->{driver}->get_driver_name(),
        backend         => $self->{backend}->get_backend_name(),
        adapter_test    => $self->{pkg_test},
    } );

    $self->debug("Adapter $adapter instanciated");
    return 1;
}



=over 4

=item C<set_cheek_lifetime>

  $hamster->set_cheek_lifetime($lifetime);

=over 4

=item C<$lifetime> (Integer)

Cheek lifetime in seconds (defaults to 86400)

=back

Set the cheek lifetime in the storage interface

=back

=cut
sub set_cheek_lifetime {
    my ($self, $lifetime) = @_;
    $lifetime ||= 86400;    # always set the defualt value if none is provided

    # deprecated warning
    $self->register_error("Using the set_cheek_lifetime() method will become deprecated in Hamster::Base version >= 4, Please use the driver interface directly");
    $self->reset_error();

    $self->{driver}->set_lifetime($lifetime);
}


=over 4

=item C<get_cheek_lifetime>

  my $lifetime = $hamster->get_cheek_lifetime();

Returns the cheek data lifetime of this Hamster.

=back

=cut
sub get_cheek_lifetime {
    my ($self) = @_;
    return $self->{driver}->get_lifetime();
}


=over 4

=item C<_return_data>

  my $unified_data = $hamster->_return_data($key, $data);

=over 4

=item C<$key>

The subkey inside of a hashref

=item C<$data>

The result data coming from the get() method of the storage interface. This method is automatically invoked by the get() method.

=back

Provides a unified data return management for every kind of data structures the get() method may return.
If the $key variable is set the data structure of $data must be a hashref where the hashkey $key must exist. Then this data is returned. 

=back

=cut
sub _return_data {
    my ($self, $key, $data) = @_;

    my $debug_key = defined $key ? $key : "undef";
    $self->debug("_return_data('$debug_key') called in ".(caller)[0]." line ".(caller)[2]);

    if ( ref $data eq "HASH" && defined $data->{data}) {

        # Hamster cheek data structure is present, data is a HASH and data keys are greater than zero
        if ( (ref $data->{data} eq "HASH") && (scalar keys %{$data->{data}} ne 0) ) {

            # subkey defined and present
            return $data->{data}->{$key} if ( (defined $key) && (defined $data->{data}->{$key}) );

            # return anything other
            return $data->{data};

        # Hamster cheek data structure is present, data is an ARRAY and array size is greater then -1 (no subkey possible)
        #   OR
        # Hamster cheek data structure is present, scalar inside 
        #   OR
        # Hamster cheek data structure is present, CFR-data
        } elsif (   (ref $data->{data} eq "ARRAY") && ($#{$data->{data}} ne -1) ||
                    (ref $data->{data} eq "cia::forensic::record") ||
                    (ref $data->{data} eq "")
        ) {

            return $data->{data};

        # anything else, we suppose it's ok to be like so...
        } else {
            return $data->{data};

        }

    # simple array and array size is greater than -1 
    } elsif ( (ref $data eq "ARRAY") && ($#{$data} ne -1) ) {

        return $data;

    # scalar inside
    } elsif (ref $data eq "") {

        return $data;

    # anything other
    } else {

        # subkey defined and present
        return $data->{$key} if ( (defined $key) && (defined $data->{$key}) );

        # return anything other
        return $data;
    }

    # rotten data
    $data = undef if ( (!defined $data) || ($data eq "") );
    return $data;
}


=over 4

=item C<is_lifetime_expired>

  if ($hamster->is_lifetime_expired($creation_date)) {
      ..
  }

=over 4

=item C<$cdate> (Integer)

The timestamp of the creation_date. 

=back

Check if the cheek lifetime has expired. Expects the first parameter to be a timestamp.
This value plus the lifetime value is compared to now.

=back

=cut
sub is_lifetime_expired {
    my ($self, $ctime) = @_;

    $ctime ||= 0;  # in case an empty creation_date is provided take 0, it will be less than the current timestamp
    return (($ctime + $self->{driver}->get_lifetime()) < time());
}


=over 4

=item C<_update_cheek_data>

  my $cheek_data = $hamster->_update_cheek_data($ident);

=over 4

=item C<$ident>

The identifier for the cheek data

=back

Updates the cheek data for $ident. This method does work driver-independent and makes sure the Hamsters cheek always have up-to-date data.

=back

=cut
sub _update_cheek_data {
    my ($self, $ident) = @_;

    my $cheek_data = undef;
    $self->debug(sprintf("Cheek data for ident '%s' are flagged invalid, reloading data", $ident));

    # Hamster has at least one backend package
    if ($self->{backend}->has_backend()) {

        $cheek_data = $self->{backend}->fill_cheek($ident);

        # check for backend package errors
        if ($self->{backend}->{backend}->has_error()) {
            $self->register_error($self->{backend}->{backend}->get_error());
            return undef;
        }

    # Hamster does not support the backend mechanism or no backend could be loaded, try the fill_cheek() method of the Hamster package
    } else {
        # check for backend errors
        if ($self->{backend}->has_error()) {
            $self->register_error($self->{name}." defined Backend '".$self->{backend}->get_backend_name()."', but Backend could not be loaded: ".$self->{backend}->get_error());
            return undef;
        }

        if (!$self->can("fill_cheek")) {
            $self->register_error($self->{name}." has no Backends available and no fill_cheek() method is present.");
            return undef;
        }

        $self->debug("Calling local fill_cheek() method");
        $cheek_data = $self->fill_cheek($ident);

        # check for backend package errors
        return undef if ($self->has_error());
    }

    $self->set($ident, $cheek_data);
    
    return $self->{driver}->get($ident);;
}


=over 4

=item C<get>

  my $data = $hamster->get($ident);

=over 4

=item C<$ident>

The identifier for the cheek data

=item C<$key>

The subkey inside of the cheek data (must be a hashref) that you want to have returned. This is optional.

=back

To describe it in a simple way, this method returns the data for $ident (and $key if provided).

In detail a lot of operations are done to make sure to have the right data:

=over 2

=item Preprocessing the identifier

If the C<_preprocess_ident()> method is available in the Hamster package the methods return value is taken as identifier. The method takes the current $ident variable as first parameter. You may do whatever you want in there.

Note that the preprocessed $ident value is taken into $self->{ident} to save time when $ident is undefined.

=item First-time get from the interface driver

The C<get()> method is invoked to get the current data from the interface driver storage system. If this return value is not a hash (special Hamster cheek format) the data is invalid. In this case the C<_update_cheek_data()> methods return value is used as the current cheek data.

=item Data validation

The cheek data is now validated, which means that $cheek_data->{data} must be present and the cheek data must not be outdated (via C<is_lifetime_expired()> method). If these validations fail the C<_update_cheek_data()> methods return value is used as the new cheek data.

=item Data returning

Then the cheek data is returned using the C<_return_data()> method. If the data is still undefined, undef will be returned.

=back

On all steps of these operations the error handling mechanisms are considered.

=back

=cut
sub get {
    my ($self, $ident, $key) = @_;
    $ident ||= $self->{ident};

    $self->debug(sprintf("Calling get('%s','%s') from %s in line %s",
        (defined $ident)    ? $ident    : "undef",
        (defined $key)      ? $key      : "NO_MORE_ARGS",
        (caller)[0],
        (caller)[2]
    ));

    # write back ident to $self->{ident}
    $self->{ident} = $ident;

    return $self->register_error("no ident defined") unless defined $ident;


    # first attempt to get data from storage
    my $result = $self->{driver}->get($ident);

    # interface error, just register this error and return the previous result
    if ($self->{driver}->has_error()) {
        $self->register_error($self->{driver}->get_error());
        return $self->_return_data($key, $result);
    }

    # define cases where we need fresh data from the backend or the hamsters fill_cheek() method
    if (    (ref $result ne "HASH") ||
            (   (ref $result eq "HASH") && (defined $result->{creation_date}) && ($self->is_lifetime_expired($result->{creation_date})) )
    ) {
        $result = $self->_update_cheek_data($ident);
    }

    return $self->_return_data($key, $result);
}


=over 4

=item C<set>

  if (!$hamster->set($ident, $data)) {
      print STDERR $hamster->get_error();
  }

=over 4

=item C<$ident>

The identifier for the cheek data

=item C<$data>

A data structure you want to save, belonging to the identifier

=back

Save data to the interface driver:

=over 2

=item Preprossing the identifier

See PREPROCESSING THE IDENTIFIER for more information

=item Storing data

The data is stored to the interface driver. In an error occurs this methods returns false (0). The error message can be obtained from $hamster->get_error().

=item Updating the adapter cache

If an adapter is available the adapters cache is updated to the new data.

=back

If everything succeeded this method retuns true (1).

=back

=cut
sub set {
    my ($self, $ident, $args) = @_;
    $ident ||= $self->{ident};

    # check if the Hamster uses the soft-set feature. If so we execute soft_set() instead of set()
    return $self->soft_set($ident, $args) if ($self->{driver}->get_soft_set() == 1);

    # go on otherwise
    $self->debug(sprintf("Calling set('%s','%s') from %s in line %s",
        (defined $ident)    ? $ident    : "undef",
        (defined $args)     ? $args     : "undef",
        (caller)[0],
        (caller)[2]
    ));

    return $self->register_error("No ident provided. Cannot operate") unless defined $ident;
    
    # execute storage set operation and return an error if that failed
    return $self->register_error($self->{driver}->get_error()) unless $self->{driver}->set($ident, $args);

    # always update adapter when new data have been set
    $self->update_adapter($self->get($ident));
    return 1;
}


=over 4

=item C<soft_set>

  $hamster->soft_set($ident, $data);

Save the data belonging to $ident to the temporary interface cache. This data will be saved when the C<commit()> method is called.

Note that the I<soft_set> feature does not use the I<Preprocessing the identifier> metchanism.

=back

=cut
sub soft_set {
    my ($self, $ident, $args) = @_;
    $ident ||= $self->{ident};

    # save the ident for the commit
    $self->{ident} = $ident;

    $self->debug(sprintf("Calling soft_set('%s','%s') from %s in line %s",
        (defined $ident)    ? $ident    : "undef",
        (defined $args)     ? $args     : "undef",
        (caller)[0],
        (caller)[2]
    ));

    return $self->register_error("No ident provided. Cannot operate") unless defined $ident;

    # soft_set operation (will always return true since the data is stored to a hash)
    $self->{driver}->soft_set($ident, $args);

    # always update adapter when new data have been set
    $self->update_adapter($self->get($ident));
    return 1;
}


=over 4

=item C<commit>

  if (!$hamster->commit()) {
      print STDERR $hamster->get_error();
  }

Commit all data previously I<saved> with C<soft_set()>. Note that the I<soft_set> feature does not use the I<Preprocessing the identifier> metchanism, so C<commit()> won't do either.

=back

=cut
sub commit {
    my ($self, $ident) = @_;
    $ident ||= $self->{ident};

    $self->debug(sprintf("Calling commit() from %s in line %s",
        (caller)[0],
        (caller)[2]
    ));

    return $self->register_error("No ident provided. Cannot operate") unless defined $ident;

    return $self->register_error($self->{driver}->get_error()) unless $self->{driver}->commit($ident);

    # always update adapter when new data have been set
    $self->update_adapter($self->get($ident));

    return 1;
}


=over 4

=item C<delete>

  if ($hamster->delete($ident)) {
      print STDERR $hamster->get_error();
  }

Delete some data identified by $ident from the driver storage.

=back

=cut
sub delete {
    my ($self, $ident) = @_;
    $ident ||= $self->{ident};

    $self->debug(sprintf("Calling delete('%s') from %s in line %s",
        (defined $ident)    ? $ident    : "undef",
        (caller)[0],
        (caller)[2]
    ));

    return $self->register_error("No ident provided") unless defined $ident;
    return $self->register_error($self->{driver}->get_error()) unless $self->{driver}->delete($ident);
    return 1;
}


=over 4

=item C<update>

  if (!$hamster->update($ident, $data)) {
      print STDERR $hamster->get_error();
  }

Update a cheek identified by $ident with $data. Note that this cheek must not necessarily defined for updating it. In that case a C<set()> command is executed. This has to be done by the driver itself.

=back

=cut
sub update {
    my ($self, $ident, $args) = @_;
    $ident ||= $self->{ident};

    $self->debug(sprintf("Calling update('%s','%s') from %s in line %s",
        (defined $ident)    ? $ident    : "undef",
        (defined $args)     ? $args     : "undef",
        (caller)[0],
        (caller)[2]
    ));

    return $self->register_error("No ident provided") unless defined $ident;
    return $self->register_error($self->{driver}->get_error()) unless $self->{driver}->update($ident, $args);
    return 1;
}



=over 4

=item C<flush>

Flush all data in the storage system. Note that in case of Cache::Memcached B<all> data in all namespaces are flushed. Be careful when using this method with the Memcached driver.

=back

=cut
sub flush {
    my ($self) = @_;

    $self->debug("Calling flush() from ".(caller)[0]." in line ".(caller)[2]);

    # interface operation
    if (!$self->{driver}->flush()) {
        $self->register_error($self->{driver}->get_error());
        return 0;
    }
    return 1;
}



=over 4

=item C<update_adapter>

  $hamster->update_adapter(@_);

=over 4

=item C<@_>

All remaining data (a hashref)

=back

Updates the adapter cache with the current identifier from $self->{ident} and @_ if there is an adapter present.

=back

=cut
sub update_adapter {
    my ($self, $data) = @_;

    return undef if ( (!defined $self->{ident}) || ($self->{ident} eq "") );

    if (defined $self->{adapter}) {
        $self->debug("update_adapter('".$self->{ident}."') called from ".(caller)[0]." in line ".(caller)[2]);

        # set the Adapters cache identifier
        $self->{adapter}->set_ident($self->{ident});

        if ( (ref $data eq "HASH") && (defined $data->{data}) ) {
            $self->{adapter}->load_cache($self->{ident}, $data->{data});
        } else {
            $self->{adapter}->load_cache($self->{ident}, $data);
        }
    }
}


=over 4

=item C<AUTOLOAD>

  my $result = $hamster->adapter_method();

Overloads the Hamster package with the adapter method. Please see the corresponding adapter method you want to call for more documentation.

Please note, that since Hamster::Base version >= 3.5 the first parameter *has to be* the ident parameter, or undef to take the default identifier.

=back

=cut
sub AUTOLOAD {
    our $AUTOLOAD;
    my ($self, $ident, @args) = @_;
    $ident ||= $self->{ident};

    my $adapter_method = ($AUTOLOAD =~ m{([^:]+$)})[0];
    $self->debug(sprintf("Magic adapter method %s('%s', '%s') called from %s in line %s",
        $adapter_method,
        $ident,
        (scalar @args) ? join(",", @args) : "undef", 
        (caller)[0], 
        (caller)[2] 
    ));

    # check if there is an adapter present
    unless (defined $self->{adapter}) {
        $self->register_error("No adapter present");
        return undef;
    }

    # check if there is an adapter present *and* if the method is available in the adapter 
    unless ($self->{adapter}->can($adapter_method) ) {
        $self->register_error(sprintf("The adapter method '%s' is not available via '%s'", $adapter_method, $self->getAdapter()));
        return undef;
    }

    # check if there are hamster data for $ident available. If not abort immediately.
    if (!defined $ident) {
        $self->register_error("Neither an ident was provided within the adapter call nor the Hamster has been initialized with an ident, cannot go on");
        return undef;
    }


    # Then make sure to have the Adapter cache filled up with some data. If the cache does not contain any data, call the fill_cheek() method to have the latest data available
    # This is best done by the get() method.
    $self->update_adapter($self->get($ident)) if (!$self->{adapter}->cache_has_data());

    # now call the method
    my $adapter_result = $self->{adapter}->$adapter_method($ident, @args);
    if ($self->{adapter}->has_error()) {
        my $previous_error = $self->get_error();
        $previous_error ||= "No previous error";
        $self->register_error("The Adapter '".$self->{adapter}->{name}."' returned an error: ".$self->{adapter}->get_error());

        # To avoid more and/or irrelevant adapter errors, reset the adapter errors
        $self->{adapter}->reset_error();
    }
    return $adapter_result;
}


=pod Package destruction (needed by AUTOLOAD)
=cut
sub DESTROY {
    my ($self) = shift;
    undef $self;
}


=head1 DEPENDENCIES 

  Carp


=head1 ERROR HANDLING

This package uses L<BaseClass> for error reporting and handling. If an error happened you may check $hamster->has_error().
The error message itself can be obtained from $hamster->get_error().

For more information read the L<BaseClass documentation|BaseClass>.


=head1 AUTHOR

Tristan Cebulla <tristan.cebulla@1und1.de>, 1&1 Internet AG


=head1 BUGS

There are no known bugs.


=head1 COPYRIGHT & LICENSE

Copyright (c) 2013 Tristan Cebulla (<tristan.cebulla@1und1.de>). All rights reserved.

This module is free software; you can redistribute it and/or modify it under the same terms as
Perl itself. See L<perlartistic>.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=cut

1;
