package Hamster::Adapter::Base;

=head1 NAME

Hamster::Adapter::Base


=head1 SYNOPSIS

  package Hamster::Adapter::MyHamsterAdapter;

  use Hamster::Adapter::Base;
  use base qw(Hamster::Adapter::Base);


=head1 DESCRIPTION

This is the Hamster Adapter base class. It provieds some default functionalities to run a Hamster adapter.

The current C<$ident> is stored in C<$self->{ident}. You B<should> use the following code to access any methods C<$ident> value (and of course some more parameters as a hashref):

=over 2

  sub method {
      my ($self) = shift;
      my ($ident) = shift;
      $ident ||= $self->{ident};
      my ($args) = @_;
  
      ...
  }

=back


=head1 DEPENDENCIES 

This package does not have real dependencies since it only operates a series of drivers. The dependencies are
specified in the driver package itself.


=head1 ADAPTER CACHING

The Adapter has it's own cache where the latest cheek data is saved that has come from an interfaces C<get()> method. Everytime this happens the Adapters cache is updated with this data via the C<load()> method.


=head1 ERROR HANDLING

This package uses L<BaseClass> for error reporting and handling. If an error happened you may check $hamster->has_error().
The error message itself can be obtained from $hamster->get_error().

For more information read the L<BaseClass documentation|BaseClass>.

=cut


use BaseClass;
use base qw(BaseClass);


=head1 METHODS

=over 4

=item C<new>

Create a new instance of this package. The constructor supports the following hash-args:

=over 4

=item driver (String)

The Hamsters interface driver (short)name.

=back

=back

=cut
sub new {
    my ($class, $args) = @_;

    my $self = bless {
        name            => $class,
        ident           => undef,
        cache           => {},
        driver_name     => $args->{driver},
        test_adapter    => $args->{test_adapter},
    }, $class;
    return $self;
}


=over 4

=item C<get_driver_name>

  my $driver_name = $adapter->get_driver_name();

Return the drivers name.

=back

=cut
sub get_driver_name {
    my ($self) = shift;
    return $self->{driver_name};
}


=over 4

=item C<load>

  $adapter->load($ident, $data);

=over 4

=item $ident

The ident string that identifies this cheek data.

=item $data

The data belonging to the ident string (used for set and update methods). This may contain any data structure you want.

=back

Update the adapters cache with the data belonging to C<$ident>.

=back

=cut
sub load_cache {
    my ($self, $ident) = (shift, shift);

    return undef if ( (!defined $ident) || ($ident eq "") );

    $self->debug("Filling up Adapter cache for ident '$ident'");

    # save the current ident string
    $self->{ident} = $ident;
    # and the cache data
    ($self->{cache}->{$ident}) = @_;
}


=over 4

=item C<cache_has_data>

  if ($adapter->cache_has_data()) {
      ..
  } else {
      ..
  }

Returns whether the Adapter cache has some data. Note that an ident must have been set before.

=back

=cut
sub cache_has_data {
    my ($self) = @_;
    return 0 unless defined $self->{ident};

    # $ident must be a hashkey and the inside-data must be defined and of type arrayref, hashref or scalar with non-zero size
    my $ident = $self->{ident};
    return 1 if (
        (defined $self->{cache}->{$ident}) &&
        (   (ref $self->{cache}->{$ident} eq "ARRAY") ||
            (ref $self->{cache}->{$ident} eq "HASH") ||
            (length($self->{cache}->{$ident}) != 0)
        )
    );
    return 0;
}



=over 4

=item C<get_ident>

  my $current_ident = $adapter->get_ident();

Returns the current C<$ident> value

=back

=cut
sub get_ident {
	my ($self) = @_;
	return $self->{ident};
}


=over 4

=item C<set_ident>

  $adapter->set_ident($ident);

=over 4

=item $ident

The ident string that identifies this cheek data.

=back

Sets the current C<$ident> value

=back

=cut
sub set_ident {
	my ($self) = shift;
	($self->{ident}) = shift;
    $self->debug("set_ident('".$self->{ident}."') called");
}


=head1 AUTHOR

Tristan Cebulla <tristan.cebulla@1und1.de>, 1&1 Internet AG


=head1 BUGS

There are no known bugs.


=head1 COPYRIGHT & LICENSE

Copyright (c) 2011 Tristan Cebulla (<tristan.cebulla@1und1.de>). All rights reserved.

This module is free software; you can redistribute it and/or modify it under the same terms as
Perl itself. See L<perlartistic>.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=cut

1;
